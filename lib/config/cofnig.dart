import 'package:flutter/services.dart';
import 'package:yaml/yaml.dart';

class ConfigNotLoadedException implements Exception {
  String _message;
  ConfigNotLoadedException(String message) : _message = message;
  String what() {
    return _message;
  }
}

class Config {
  dynamic __config;
  Future<void> loadFromAsset() async {
    String rawConfig =
        await rootBundle.loadString('assets/config/app_config.yaml');
    __config = loadYaml(rawConfig);
  }

  void loadFromString(String config) {
    __config = loadYaml(config);
  }

  operator[](dynamic key) {
    if (__config == null) {
      throw ConfigNotLoadedException("Config not loaded!");
    }
    return __config[key];

  }
}
