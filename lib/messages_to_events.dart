import 'package:game/game_communication/generated/lib/game_communication/messages.pbserver.dart'
    as Proto;
import 'package:game/game_logic/game_engine/game_event.dart';
import 'package:game/game_logic/events/offline_game_started_request_received_event.dart';

class InvalidMessageToEventMapping implements Exception {
  String _message;
  InvalidMessageToEventMapping({String message = ""}) : _message = message;
  String what() {
    return _message;
  }
}

// ------------------ mappings ----------------------

GameEvent playerJoined(Proto.Gui2LoicMessage playerJoinedMessage) {
  if (playerJoinedMessage is Proto.PlayerJoined == false) {
    throw InvalidMessageToEventMapping();
  }
  return null;
}

GameEvent offlineGameStarted(Proto.Gui2LoicMessage message) {
  if (message.hasOfflineGameStartedRequest() == false) {
    throw InvalidMessageToEventMapping();
  }
  return OfflineGameStaredRequestReceivedEvent(
    gameId: message.offlineGameStartedRequest.gameId,
    playerName: message.offlineGameStartedRequest.playerName,
  );
}
