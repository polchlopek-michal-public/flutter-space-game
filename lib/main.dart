import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:game/application_mode.dart';
import 'package:game/config/cofnig.dart';
import 'package:game/game_communication/game_communication.dart';
import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_gui/communication/offline_logic_communication.dart';
import 'package:game/game_gui/game_app.dart';
import 'package:game/game_logic/communication/utils/game_event_from_message_factory.dart';
import 'package:game/game_logic/communication/offline_gui_communication.dart';
import 'package:game/game_logic/components/health.dart';
import 'package:game/game_logic/components/position.dart';
import 'package:game/game_logic/components/sprite.dart';
import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/components_manager/components_register.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_register.dart';
import 'package:game/game_logic/components/velocity.dart' as Component;
import 'package:game/game_logic/game_engine/game_events_manager/game_events_manager.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_register.dart';

import 'package:game/game_logic/game_engine/game_engine.dart';
import 'package:game/game_logic/game_engine/systems_manager/system_manager.dart';
import 'package:game/game_logic/game_engine/systems_manager/systems_register.dart';
import 'package:game/game_logic/registers/custom_game_event_regsiter.dart';
import 'package:game/game_logic/systems/render_system.dart';
import 'package:game/game_persistance/database/i_database.dart';
import 'package:game/game_persistance/database/sqlite_database.dart';
import 'package:game/game_persistance/game_database_persistance.dart';
import 'package:game/game_persistance/i_game_persistance.dart';
import 'package:game/messages_to_events.dart' as MessagesToEvents;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  const ApplicationMode mode =
      ApplicationMode.OFFLINE; //TODO: set it from dart file run argument
  Config config;
  GameCommunication guiCommunication;
  GameCommunication logicCommunication;

  // ---------- loading config --------------------
  config = Config();
  await config.loadFromAsset();
  // ----------------------------------------------

  // ---------- setting communication -------------
  // ignore: close_sinks
  StreamController<Uint8List> offlineLogic2GuiStreamController =
      StreamController<Uint8List>();
  // ignore: close_sinks
  StreamController<Uint8List> offlineGui2LogicStreamController =
      StreamController<Uint8List>();

  if (mode == ApplicationMode.OFFLINE) {
    guiCommunication = OfflineGuiCommunication(
      rawMessagesSink: offlineLogic2GuiStreamController,
      rawMessagesStream: offlineGui2LogicStreamController.stream,
    );
    logicCommunication = OfflineLogicCommunication(
      rawMessagesSink: offlineGui2LogicStreamController,
      rawMessagesStream: offlineLogic2GuiStreamController.stream,
    );
  } else if (mode == ApplicationMode.ONLINE) {
    //TODO: implement...
  } else {
    throw UnknownApplicationMode("Message mode ${mode.toString()} unknown.");
  }
  // ----------------------------------------------

  // ---------- game domain ---------------------
  IDatabase database = SQLiteDatabase(
      path:
          'assets/persistance/testdatabase.db'); // TODO: can't use sqflite for web app - unsupported
  await database.open();
  IGamePersistance gamePersistance =
      GameDatabasePersistance(database: database);

  // testing: print all entiites and component in database (to delete)
  // print(await database.execute("SELECT * FROM components;"));
  // print(await database.execute("SELECT * FROM entities;"));
  // ----------------------------------------------

  // ---------- start engine --------------------
 GameEngine logicEngine;
  EntitiesRegister entitiesRegister =
      EntitiesRegister(config: config); // register from config
  ComponentsRegister componentsRegister =
      ComponentsRegister() // register given types
        ..register<Component.Velocity>()
        ..register<Health>()
        ..register<Sprite>()
        ..register<Position>();
  SystemsRegister systemsRegister = SystemsRegister()
    ..register<RenderSystem>(RenderSystem());
  GameEventsRegister gameEventsRegister =
      CustomGameEventRegister(gamePersistance: gamePersistance);

  GameEventFromMessageFactory gameEventFromMessageFactory =
      GameEventFromMessageFactory()
        ..assignMethod(
          Proto.Gui2LoicMessage_Paylaod.playerJoined,
          MessagesToEvents.playerJoined,
        )
        ..assignMethod(
          Proto.Gui2LoicMessage_Paylaod.offlineGameStartedRequest,
          MessagesToEvents.offlineGameStarted,
        );

  EntitiesManager entitiesManager =
      EntitiesManager(entitiesRegister: entitiesRegister, config: config);

  ComponentsManager componentsManager =
      ComponentsManager(config: config, componentsRegister: componentsRegister);

  SystemManager systemManager = SystemManager(systemsRegister: systemsRegister);

  GameEventsManager gameEventsManager =
      GameEventsManager(gameEventsRegister: gameEventsRegister);

  guiCommunication.setOnRecvHandler((Proto.Message message) {
    Proto.Gui2LoicMessage gui2logicMessage = message.gui2LoicMessage;
    gameEventsManager.event(gameEventFromMessageFactory.create(
        gui2logicMessage.whichPaylaod(), gui2logicMessage));
  });

  logicEngine = GameEngine(
    config: config,
    entitiesManager: entitiesManager,
    componentsManager: componentsManager,
    systemManager: systemManager,
    gameEventsManager: gameEventsManager,
    guiCommunication: guiCommunication,
  );

  logicEngine.run();
  // --------------------------------------------

  // ---------- start app --------------------
  runApp(GameApp(logicCommunication: logicCommunication));
  // -----------------------------------------
}
