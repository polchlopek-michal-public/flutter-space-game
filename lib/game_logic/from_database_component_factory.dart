import 'package:game/game_logic/components/position.dart';
import 'package:game/game_logic/components/sprite.dart';
import 'package:game/game_logic/game_engine/component.dart';
import 'package:game/game_logic/components/health.dart';

/// Creates component basing on data return from Database.
/// Data are passed to [createFromDataBase] function as [Map<String,dynamic>]
class FromDatabaseComponentFactory {
  Map<String, Component Function(Map<String, dynamic>)> __factories = {};

  FromDatabaseComponentFactory() {
    __factories["Health"] = (Map<String, dynamic> data) {
      return Health(
        maxValue: data["maxValue"],
        value: data["value"],
      );
    };
    __factories["Position"] = (Map<String, dynamic> data) {
      return Position(
        x: data["x"],
        y: data["y"],
      );
    };

    __factories["Sprite"] = (Map<String, dynamic> data) {
      return Sprite(
        state: data["state"],
        scale: data["scale"],
      );
    };
  }

  /// Creates component of wanted type from decoded database data.
  Component create(String type, dynamic data) {
    return __factories[type](data as Map<String, dynamic>);
  }
}
