import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_logic/game_engine/game_event.dart';
import 'package:protobuf/protobuf.dart' as Proto;

class GameEventFactoryMethodNotRegistered implements Exception {
  String _message;
  GameEventFactoryMethodNotRegistered(String message) : _message = message;
  String what() {
    return _message;
  }
}

/// Factory of GameEvents
/// Use factory methods assigned to instance of class
/// by [assignMethod] to create GameEvent based on [Proto.Gui2LogicMesage].
class GameEventFromMessageFactory {
  Map<Proto.Gui2LoicMessage_Paylaod,
      GameEvent Function(Proto.Gui2LoicMessage message)> __factories = {};

  /// Assigns factory method
  /// Overrides if exist.
  void assignMethod(Proto.Gui2LoicMessage_Paylaod which,
      GameEvent Function(Proto.Gui2LoicMessage message) factoryMethod) {
    __factories[which] = factoryMethod;
  }

  /// Creates GameEvent from message
  /// throws [GameEventFactoryMethodNotRegistered] if factory method not registed.
  GameEvent create(
      Proto.Gui2LoicMessage_Paylaod which, Proto.Gui2LoicMessage message) {
    if (__factories.keys.contains(which) == false)
      throw GameEventFactoryMethodNotRegistered(
          "Factory method of $which type not registered!");
    return __factories[which](message);
  }
}
