abstract class ProtoSerializableComponent {
  dynamic toProtoComponent();
}