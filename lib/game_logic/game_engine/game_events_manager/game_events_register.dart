import 'package:game/game_communication/game_communication.dart';
import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_logic/game_engine/game_event.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_manager.dart';
import 'package:game/game_logic/game_engine/systems_manager/i_system_manager.dart';

class GameEventHandlerNotFound implements Exception {
  String _message;
  GameEventHandlerNotFound(String message) : _message = message;
  String what() {
    return _message;
  }
}

typedef void GameEventHandler(
  dynamic event,
  EntitiesManager entitiesManager,
  ComponentsManager componentsManager,
  ISystemManager systemManager,
  GameEventsManager gameEventsManager,
  GameCommunication gameCommunication,
);

/// Holds registered [GameEvent]s
/// On base of registered events [EventsManager] handles game events.
class GameEventsRegister {
  Map<Type, GameEventHandler> registered = {};

  /// Regsiter component of any type inherited from [Component] class.
  /// Be carefull with overriding.
  /// Beware of using it with static type interpretation and polimorphial type!
  GameEventsRegister register<EventType extends GameEvent>(
      GameEventHandler handler) {
    registered[EventType] = handler;
    return this;
  }

  /// Returns handler of given event
  GameEventHandler handler(GameEvent event) {
    if (registered.containsKey(event.runtimeType) == false) {
      throw GameEventHandlerNotFound(
          "Handler of ${event.runtimeType} event not found!");
    }
    return registered[event.runtimeType];
  }
}
