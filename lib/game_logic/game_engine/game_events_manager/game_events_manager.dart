import 'package:meta/meta.dart';
import 'package:game/game_communication/game_communication.dart';
import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_logic/game_engine/game_event.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_register.dart';
import 'package:game/game_logic/game_engine/systems_manager/i_system_manager.dart';

class GameEventsManager {
  GameEventsRegister gameEventsRegister;
  List<GameEvent> __gameEvents = [];
  GameEventsManager({@required this.gameEventsRegister});

  void consume(
    EntitiesManager entitiesManager,
    ComponentsManager componentsManager,
    ISystemManager systemManager,
    GameEventsManager gameEventsManager,
    GameCommunication guiCommunication,
  ) {
    __gameEvents.forEach((GameEvent event) {
      gameEventsRegister.handler(event)(
        event,
        entitiesManager,
        componentsManager,
        systemManager,
        this,
        guiCommunication,
      );
    });
    __gameEvents.clear();
  }

  void event(GameEvent event) {
    __gameEvents.add(event);
  }
}
