class SystemNotFound implements Exception {
  String _message;
  SystemNotFound(String message) : _message = message;
  String what() {
    return _message;
  }
}