import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_manager.dart';
import 'package:game/game_logic/game_engine/systems_manager/system_manager_errors.dart';
import 'package:game/game_logic/game_engine/systems_manager/systems_register.dart';
import 'package:meta/meta.dart';
import 'package:game/game_logic/game_engine/system.dart';
import 'package:game/game_logic/game_engine/systems_manager/i_system_manager.dart';

class SystemManager implements ISystemManager {
  SystemsRegister systemsRegister;

  SystemManager({@required this.systemsRegister});

  @override
  void update(
    EntitiesManager entitiesManager,
    ComponentsManager componentsManager,
    GameEventsManager gameEventsManager,
  ) {
    // Updates all system with [gameEventsManager] passed as param:
    systemsRegister.registered.values.forEach((System system) {
      system.update(
        entitiesManager,
        componentsManager,
        gameEventsManager,
      );
    });
  }

  /// Attaches new system
  /// If system alreade attached - overrides it.
  @override
  void attach<T extends System>(T system) {
    systemsRegister.registered[T] = system;
  }

  @override
  void detach<T extends System>() {
    if (systemsRegister.registered.keys.contains(T)) {
      systemsRegister.registered.remove(T);
      //? Handle detaching non-exisitng system?
    }
  }

  @override
  T system<T extends System>() {
    if (systemsRegister.registered.keys.contains(T)) {
      return systemsRegister.registered[T];
    } else {
      throw SystemNotFound("System of type $T not found!");
    }
  }
}
