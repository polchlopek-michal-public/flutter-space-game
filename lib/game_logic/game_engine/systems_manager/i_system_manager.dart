import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_manager.dart';
import 'package:game/game_logic/game_engine/system.dart';

abstract class ISystemManager {
  /// Updates all systems in [SystemManager].
  void update(
    EntitiesManager entitiesManager,
    ComponentsManager componentsManager,
    GameEventsManager gameEventsManager,
  );

  /// Attaches [system] to [SystemManager].
  /// Beware of using it with static type interpretation and polimorphial type!
  /// then it will attach to [System] isntead of runtime type!
  void attach<T extends System>(T system);

  /// Detaches runnig [system] of [T] if exists in [SystemManager]
  void detach<T extends System>();

  /// Return [System] of given [T] type.
  T system<T extends System>();
}
