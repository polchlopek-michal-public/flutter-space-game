import 'package:game/game_logic/game_engine/system.dart';

/// Holds registered Systems
/// On base of registered components [SystemManager] is created.
/// To be honest [SystemsRegister] is used only for elegant initalization
/// and is oblived after [SystemManager] constructor invocation...
/// Attaching or Detaching [systems] are then still possible in runtime by [SystemsManager].
class SystemsRegister {
  Map<Type, System> registered = {};

  /// Regsiter [system] of type [T] inherited from [System] class.
  SystemsRegister register<T extends System>(T system) {
    if (registered.keys.contains(T) == false) {
      registered[T] = system;
    }
    return this;
  }
}
