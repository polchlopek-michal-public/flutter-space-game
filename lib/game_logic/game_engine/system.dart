import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_manager.dart';

abstract class System {
  void update(
    EntitiesManager entitiesManager,
    ComponentsManager componentsManager,
    GameEventsManager gameEventsManager,
  );
}
