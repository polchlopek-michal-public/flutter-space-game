import 'package:game/game_communication/game_communication.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_manager.dart';
import 'package:meta/meta.dart';
import 'package:game/config/cofnig.dart';
import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/systems_manager/system_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';

class GameEngine {
  Config config;
  EntitiesManager entitiesManager;
  ComponentsManager componentsManager;
  SystemManager systemManager;
  GameEventsManager gameEventsManager;
  GameCommunication guiCommunication;

  GameEngine({
    @required this.config,
    @required this.entitiesManager,
    @required this.componentsManager,
    @required this.systemManager,
    @required this.gameEventsManager,
    @required this.guiCommunication,
  });

  void run() async {
    while (true) {
      gameEventsManager.consume(
        entitiesManager,
        componentsManager,
        systemManager,
        gameEventsManager,
        guiCommunication,
      );
      systemManager.update(
        entitiesManager,
        componentsManager,
        gameEventsManager,
      );
      await new Future.delayed(const Duration(milliseconds: 75));
    }
  }
}
