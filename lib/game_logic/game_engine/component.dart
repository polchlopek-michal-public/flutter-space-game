/// Component is utility of entity
/// One entity can have multiple components
///
/// Components holds data which can be processed by systems
/// or on event occurance.
abstract class Component {}
