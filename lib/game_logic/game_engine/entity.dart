/// Reperesents any object in game logic.
/// Contains only id.
/// Id shhould be set after adding to entities manager.
class Entity {
  int id;
  String type;

  /// Creates Entitiy with given id and type
  /// If id will be not null durring adding to entities manager
  /// Manager can reject it, because id will not fit with [type] according to type registered in [EntitiesRegister]. 
  Entity({this.id, this.type});

  /// Sets id of entity
  /// Id should be set by EntitiesManager on adding this entitiy to manager.
  /// other way type of entiity can be false.
  void setId(int id) {
    this.id = id;
  }

  int getId() {
    return id;
  }
}
