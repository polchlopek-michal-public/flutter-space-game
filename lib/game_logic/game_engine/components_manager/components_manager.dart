import 'package:meta/meta.dart';
import 'package:game/config/cofnig.dart';
import 'package:game/game_logic/game_engine/components_manager/components_register.dart';
import 'package:game/game_logic/game_engine/component.dart';

class ComponentNotFound implements Exception {
  String _message;
  ComponentNotFound(String message) : _message = message;
  String what() {
    return _message;
  }
}

class ComponentPoolNotRegistered implements Exception {
  String _message;
  ComponentPoolNotRegistered(String message) : _message = message;
  String what() {
    return _message;
  }
}

class ComponentsManager {
  Config config;
  ComponentsRegister componentsRegister;
  Map<Type, List<dynamic>> __components = {};
  ComponentsManager(
      {@required this.config, @required this.componentsRegister}) {
    componentsRegister.registered.forEach((Type type) {
      __components[type] =
          List<Component>.filled(config["entities_limit"], null);
    });
  }

  /// Adding new component.
  /// Only one type of component can be assigned to entitiy of given [entityId].
  /// Otherwise component of givent type would be overriten.
  void add(Component component, int entityId) {
    if (__components.containsKey(component.runtimeType) == false) {
      throw ComponentPoolNotRegistered(
          "Component of type ${component.runtimeType} hasnt been registered!");
    }
    //?: handling overriding?
    //?: handling OutOfRange or keep default exception?
    __components[component.runtimeType][entityId] = component;
  }

  /// Returns list of all components of [T] type.
  /// Function returns only non null elelements.
  dynamic components<T extends Component>() {
    return __components.containsKey(T)
        ? __components[T].where((component) => component != null)
        : null;
  }

  /// Returns all components of entity with given [entityId].
  /// Highly unefficient - use in the last resort!
  List<dynamic> componentsOfEntity(int entityId) {
    List<dynamic> entitiesComponents = [];
    __components.forEach((Type type, List<dynamic> components) {
      if (__components[type][entityId] != null)
        entitiesComponents.add(__components[type][entityId]);
    });
    return entitiesComponents;
  }

  /// Returns compoennt of [T] type of entiity with [entityId].
  dynamic component<T extends Component>(int entityId) {
    try {
      return __components.containsKey(T)
          ? __components[T][entityId]
          : throw ComponentNotFound;
    } catch (e) {
      throw ComponentNotFound(
          "Component of ${T.runtimeType} with $entityId id doesn't exists!");
    }
  }

  /// Removes component of [T] type of entity wit [entityId].
  void removeComponent<T extends Component>(int entityId) {
    if (__components.containsKey(T) == null) {
      return;
    }
    try {
      __components[T][entityId] = null;
    } catch (e) {}
  }

  /// Removes all components of entitiy with given [entiityId].
  void removeComponentsOfEntity(int entityId) {
    __components.keys.forEach((Type componentType) {
      try {
        __components[componentType][entityId] = null;
      } catch (e) {}
    });
  }
}
