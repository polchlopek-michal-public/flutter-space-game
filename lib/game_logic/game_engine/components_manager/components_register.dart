import 'package:game/game_logic/game_engine/component.dart';

/// Holds registered Components
/// On base of registered components [ComponentsManager] is created.
class ComponentsRegister {
  List<Type> registered = [];

  /// Regsiter component of any type inherited from [Component] class.
  /// Beware of using it with static type interpretation and polimorphial type!
  ComponentsRegister register<T extends Component>() {
    if (registered.contains(T) == false) {
      registered.add(T);
    }
    return this;
  }
}
