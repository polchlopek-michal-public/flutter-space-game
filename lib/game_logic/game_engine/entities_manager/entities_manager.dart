import 'package:meta/meta.dart';
import 'package:game/config/cofnig.dart';
import 'package:game/game_logic/game_engine/entity.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager_errors.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_register.dart';
import 'package:game/game_logic/game_engine/utils/id_generator.dart';

/// Entities manager hold all entities.
///
/// Place in memory is reserved for all future or already existing
/// entiites by its id, enitiy is being hold in list indexed
/// by its entity. Adding entity with id greater than limit is forbiden
/// ([RangeError] will be thrown).
class EntitiesManager {
  EntitiesRegister entitiesRegister;
  Config config;
  IdGenerator idGenerator;
  List<Entity> __entities;

  EntitiesManager({@required this.entitiesRegister, @required this.config}) {
    __entities = List.filled(entitiesRegister.nextAvaiableId, null);
    idGenerator = IdGenerator(
      begin: 0,
      end: config["entities_limit"],
    );
  }

  /// Creates entity and adds it to manager.
  Entity newEntity([String type = "Unknown"]) {
    int id = entitiesRegister.registered[type].generator.generate();
    Entity entity = Entity(id: id);
    return addOrUpdateEntity(entity);
  }

  /// Adds it to manager.
  /// throws UpdatingExisitngEntity if entity of given id exists
  Entity addEntity(Entity entity) {
    if (__entities[entity.id] != null) {
      throw UpdatingExisitngEntity(
          "Entity with id:{${entity.id}} has benn updated!");
    }
    return addOrUpdateEntity(entity);
  }

  /// Adds or update entity in manager.
  Entity addOrUpdateEntity(Entity entity) {
    if (entity.id == null) {
      throw AddingEntitiyWithoutId(
          "Entity without id cant be added to EntitiesManager");
    }
    if (entity.type != null) {
      if (entitiesRegister.typeof(entity.id) != entity.type) {
        throw TypeIncompatibilityError(
            "Entity of explicit type {${entity.type}} with id {${entity.id}}"
            "can't be added, because registered type of given id "
            "is {${entitiesRegister.typeof(entity.id)}}");
      }
    }

    __entities[entity.id] = entity;
    entity.type = entitiesRegister.typeof(entity.id);
    return entity;
  }

  /// Removes enitity from manager
  void remove(int id) {
    __entities[id] = null;
    entitiesRegister.registered[entitiesRegister.typeof(id)].generator
        .freeUp(id);
    // TODO: inform components manager...
  }

  /// Returns enity of given id.
  /// throws [EntityNotFound] if entity doesnt exists.
  Entity entity(int id) {
    try {
      Entity entity = __entities[id];
      if (entity == null) {
        throw EntityNotFound("Entiity with id:{$id} not found!");
      }
      return entity;
    } on RangeError {
      throw EntityNotFound("Given id is greater than manager capability");
    }
  }

  /// Returns non null entities list.
  List<Entity> entities() {
    return __entities.where((element) => element != null).toList();
  }
}
