class EntityManagerException implements Exception {
  String _message;
  EntityManagerException(String message) : _message = message;
  String what() {
    return _message;
  }
}

class UpdatingExisitngEntity extends EntityManagerException {
  UpdatingExisitngEntity(String message) : super(message);
}

class EntityNotFound extends EntityManagerException {
  EntityNotFound(String message) : super(message);
}

class AddingEntitiyWithoutId extends EntityManagerException {
  AddingEntitiyWithoutId(String message) : super(message);
}


class TypeIncompatibilityError extends EntityManagerException {
  TypeIncompatibilityError(String message) : super(message);
}
