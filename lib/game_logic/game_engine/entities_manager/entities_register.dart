import 'package:meta/meta.dart';
import 'package:game/config/cofnig.dart';
import 'package:game/game_logic/game_engine/utils/id_generator.dart';

class RegistrationError implements Exception {
  String _message;
  RegistrationError(String message) : _message = message;
  String what() {
    return _message;
  }
}

class RegisteredType {
  IdGenerator generator;
  int begin;
  int end;

  RegisteredType({this.begin, this.end}) {
    generator = IdGenerator(begin: begin, end: end);
  }
}

/// Holds types of entities with their id generators
/// Entities are registered using yaml config.
class EntitiesRegister {
  Config config;
  Map<String, RegisteredType> registered = {};
  int nextAvaiableId = 0;

  EntitiesRegister({@required this.config}) {
    __registerEntities();
  }

  IdGenerator generator(String type) {
    return registered[type].generator;
  }

  /// Returns type of entity of given id.
  /// Entity doesn't have to exists. Function jus checks membership of id.
  /// Returns [null] if id out of any range.
  String typeof(int id) {
    String type;
    registered.forEach((key, value) {
      if (id >= value.begin && id <= value.end) {
        type = key;
        return;
      }
    });
    return type;
  }

  // ----------------------------------------

  /// Registering all entiities defined in config
  /// Overflow of id wll be filled by "Unnknown" type
  /// with id - up to [entities_limit] id.
  void __registerEntities() {
    // Registering entities:
    try {
      // registered types from config:
      for (var element in config["entities"]) {
        // limit:
        registered[element["type"]] = RegisteredType(
          begin: nextAvaiableId,
          end: nextAvaiableId + element["limit"] - 1,
        );
        nextAvaiableId += element["limit"];
        if (nextAvaiableId > config["entities_limit"])
          throw RegistrationError(
              "Registration of ${element["type"]} out of range - greater than entities_limit value!}");
      }
      // registering unknown entities:
      registered["Unknown"] = RegisteredType(
        begin: nextAvaiableId,
        end: config["entities_limit"] - 1,
      );
      nextAvaiableId = config["entities_limit"];
    } catch (e) {
      throw RegistrationError("Registration can't be done!");
    }
  }
}
