import 'package:meta/meta.dart';

class IdGeneratorRangeError implements Exception {
  String _message;
  IdGeneratorRangeError(String message) : _message = message;
  String what() {
    return _message;
  }
}

/// Generates id in the range <[begin;end]>
class IdGenerator {
  int begin;
  int end;
  int __id;

  List<int> __vacated = [];
  List<int> __reserved = [];

  IdGenerator({@required this.begin, @required this.end}) {
    __id = this.begin;
  }

  /// Generate next id function.
  ///
  /// Simply returns actual possible value
  /// and increments [__id] to next possible value.
  ///
  /// If some id has been vacated with [freeUp] function
  /// then takes vacated value from [__vacated] list.
  int generate() {
    int id;
    if (__vacated.isNotEmpty) {
      id = __vacated.last;
      __vacated.removeLast();
    } else {
      __id = __ommitReserved(__id);
      id = __id;
      __id++;
    }

    return id;
  }

  /// Free up id.
  /// Vacated id will be added to [__vacated] list.
  void freeUp(int id) {
    if (id < begin || id > end) {
      throw IdGeneratorRangeError(
          "Given id: {$id} out of range of generator: <$begin;$end>");
    }
    if (__reserved.contains(id)) __reserved.remove(id);
    __vacated.add(id);
  }

  void reserve(int id) {
    if (id < begin || id > end) {
      throw IdGeneratorRangeError(
          "Given id: {$id} out of range of generator: <$begin;$end>");
    }
    if (id > __id) {
      __reserved.add(id);
    }
  }

  __ommitReserved(int id) {
    if (__reserved.contains(id)) {
      print("Ommiting reserved value during generating id");
      id = __ommitReserved(id + 1);
    }
    return id;
  }
}
