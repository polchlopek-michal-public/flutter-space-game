import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_logic/communication/proto_serializable_component.dart';
import 'package:meta/meta.dart';
import 'package:game/game_logic/game_engine/component.dart';

class Sprite extends Component implements ProtoSerializableComponent{
  final String state;
  final double scale;

  Sprite({
    @required this.state,
    this.scale = 1.0,
  });

  @override
  Proto.Sprite toProtoComponent() {
    Proto.Sprite sprite = Proto.Sprite();
    sprite.state = state;
    sprite.scale = scale;
    return sprite;
  }
}
