import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_logic/communication/proto_serializable_component.dart';
import 'package:game/game_logic/game_engine/component.dart';

class Position extends Component implements ProtoSerializableComponent {
  double x;
  double y;

  Position({
    this.x = 0.0,
    this.y = 0.0,
  });

  @override
  Proto.Position toProtoComponent() {
    Proto.Position position = Proto.Position();
    position.x = x;
    position.y = y;
    return position;
  }
}
