import 'package:vector_math/vector_math.dart';
import 'package:game/game_logic/game_engine/component.dart';

class Velocity extends Component {
  static String componentClassId = "Velocity";
  Vector2 velocity;
  double speed;

  Velocity({double x = 0, double y = 0, this.speed = 0}) {
    velocity = Vector2(x, y);
  }

  double getXCoordinate() {
    return velocity.x;
  }

  double getYCoordinate() {
    return velocity.y;
  }

  void setCoordinates({double x, double y}) {
    if (x == null && y == null) {
      throw ArgumentError(
          "At least one of arguments is reqauired and must me not equal to null.");
    }
    if (x != null) {
      velocity.x = x;
    }
    if (y != null) {
      velocity.y = y;
    }
  }
}
