import 'package:meta/meta.dart';
import 'package:game/game_logic/game_engine/component.dart';

class Health extends Component {
  double maxValue = 0;
  double value = 0;

  Health({@required this.maxValue, @required this.value});
}
