import 'package:game/game_logic/components/position.dart';
import 'package:game/game_logic/components/sprite.dart';
import 'package:meta/meta.dart';
import 'package:game/game_logic/game_engine/entity.dart';
import 'package:game/game_logic/game_engine/game_event.dart';

class RenderPackElement {
  Entity entity;
  Sprite sprite;
  Position position;

  RenderPackElement(
      {@required this.entity, @required this.sprite, @required this.position});
}

class RenderEvent extends GameEvent {
  List<RenderPackElement> renderPack = [];
  RenderEvent({@required this.renderPack});
}
