import 'package:meta/meta.dart';
import 'package:game/game_logic/game_engine/game_event.dart';

class InputMoveEvent extends GameEvent {
  double angle;
  double speed;
  InputMoveEvent({@required this.angle, @required this.speed});
}
