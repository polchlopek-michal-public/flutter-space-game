import 'package:meta/meta.dart';
import 'package:game/game_logic/game_engine/game_event.dart';

class AttachCameraTargetEvent extends GameEvent {
  int cameraTargetId;
  AttachCameraTargetEvent({@required this.cameraTargetId});
}
