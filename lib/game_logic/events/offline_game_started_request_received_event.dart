import 'package:meta/meta.dart';
import 'package:game/game_logic/game_engine/game_event.dart';

class OfflineGameStaredRequestReceivedEvent extends GameEvent {
  int gameId;
  String playerName;
  OfflineGameStaredRequestReceivedEvent({
    @required this.gameId,
    @required this.playerName,
  });
}
