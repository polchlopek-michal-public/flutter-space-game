import 'package:game/game_logic/game_engine/component.dart';
import 'package:game/game_logic/components/position.dart';
import 'package:game/game_logic/components/sprite.dart';
import 'package:game/game_logic/events/render_event.dart';
import 'package:game/game_logic/game_engine/entity.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/system.dart';

/// RenderSystem
/// Creates [RenderEvent] with renderPack
/// RenderPack should contain [Entity] and with associated
/// components necessery to render them.
class RenderSystem implements System {
  @override
  void update(
      EntitiesManager entitiesManager,
      ComponentsManager componentsManager,
      GameEventsManager gameEventsManager) {
    // Prepare render pack:
    List<RenderPackElement> renderPack = [];
    entitiesManager.entities().forEach((Entity entity) {
      try {
        Sprite sprite = componentsManager.component<Sprite>(entity.id);
        Position position = componentsManager.component<Position>(entity.id);
        RenderPackElement element = RenderPackElement(
          entity: entity,
          sprite: sprite,
          position: position,
        );
        renderPack.add(element);
      } on ComponentNotFound {
        // ignoring...
      }
    });

    // Invoke render event:
    gameEventsManager.event(RenderEvent(renderPack: renderPack));
  }
}
