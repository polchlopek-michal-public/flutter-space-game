import 'package:game/game_communication/generated/lib/game_communication/messages.pbjson.dart';
import 'package:game/game_logic/communication/proto_serializable_component.dart';
import 'package:game/game_logic/events/attach_camera_target_event.dart';
import 'package:game/game_logic/events/render_event.dart';
import 'package:meta/meta.dart';
import 'package:game/game_communication/game_communication.dart';
import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_logic/events/offline_game_started_request_received_event.dart';
import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_manager.dart';
import 'package:game/game_logic/game_engine/systems_manager/i_system_manager.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_register.dart';
import 'package:game/game_persistance/i_game_persistance.dart';

class CustomGameEventRegister extends GameEventsRegister {
  IGamePersistance gamePersistance;
  CustomGameEventRegister({
    @required this.gamePersistance,
  }) : super() {
    register<OfflineGameStaredRequestReceivedEvent>(
        __offlineGameStartedRequestReceivedEventHandler);
    register<RenderEvent>(__renderEventHandler);
    register<AttachCameraTargetEvent>(__attachCameraTargetEventHandler);
  }

  // -------------------------------------------------------------------------
  void __offlineGameStartedRequestReceivedEventHandler(
      dynamic event,
      EntitiesManager entitiesManager,
      ComponentsManager componentsManager,
      ISystemManager systemManager,
      GameEventsManager gameEventsManager,
      GameCommunication gameCommunication) {
    OfflineGameStaredRequestReceivedEvent renderEvent;
    try {
      renderEvent = event as OfflineGameStaredRequestReceivedEvent;
    } on TypeError {
      print("Wrong event type!");
    }
    gamePersistance
        .load(
      gameId: renderEvent.gameId,
      entitiesManager: entitiesManager,
      componentsManager: componentsManager,
    )
        .then((_) {
      // attaching target to camera:
      gameEventsManager.event(AttachCameraTargetEvent(cameraTargetId: 0));
      
      // sending response:
      Proto.Message message = Proto.Message();
      message.logic2GuiMessage = Proto.Logic2GuiMessage();
      message.logic2GuiMessage.offlineGameStarted = Proto.OfflineGameStarted();
      gameCommunication.send(message);


    });
  }

  void __renderEventHandler(
      dynamic event,
      EntitiesManager entitiesManager,
      ComponentsManager componentsManager,
      ISystemManager systemManager,
      GameEventsManager gameEventsManager,
      GameCommunication guiCommunication) {
    RenderEvent renderEvent;
    try {
      renderEvent = event as RenderEvent;
    } on TypeError {
      print("Wrong event type!");
    }

    Proto.Message message = Proto.Message();
    message.logic2GuiMessage = Proto.Logic2GuiMessage();
    message.logic2GuiMessage.renderPack = Proto.RenderPack();
    renderEvent.renderPack.forEach((RenderPackElement element) {
      Proto.RenderPackElement renderPackElement = Proto.RenderPackElement();
      renderPackElement.entityId = element.entity.id;
      try {
        renderPackElement.entityId = element.entity.id;
        renderPackElement.position = element.position.toProtoComponent();
        renderPackElement.sprite = element.sprite.toProtoComponent();
      } catch (e) {
        print(
            "Given component doesn't implement ProtoSerializableComponent interface!");
      }
      message.logic2GuiMessage.renderPack.elements.add(renderPackElement);
    });
    guiCommunication.send(message);
  }

  void __attachCameraTargetEventHandler(
      dynamic event,
      EntitiesManager entitiesManager,
      ComponentsManager componentsManager,
      ISystemManager systemManager,
      GameEventsManager gameEventsManager,
      GameCommunication guiCommunication) {
    AttachCameraTargetEvent attachCameraTargetEvent;
    try {
      attachCameraTargetEvent = event as AttachCameraTargetEvent;
    } on TypeError {
      print("Wrong event type!");
    }

    Proto.Message message = Proto.Message();
    message.logic2GuiMessage = Proto.Logic2GuiMessage();
    message.logic2GuiMessage.attachCameraTarget = Proto.AttachCameraTarget();
    message.logic2GuiMessage.attachCameraTarget.targetId =
        attachCameraTargetEvent.cameraTargetId;
    guiCommunication.send(message);
  }
}
