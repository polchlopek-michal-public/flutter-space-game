import 'package:meta/meta.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:game/game_persistance/database/i_database.dart';

class SQLiteDatabase implements IDatabase {
  String path;
  sqflite.Database database;
  SQLiteDatabase({@required this.path});

  @override
  Future<void> open() async {
    database = await sqflite.openDatabase(
      join(await sqflite.getDatabasesPath(), path),
      onCreate: (db, version) {
        //TODO: update creates remove inserts database works will end:
        db.rawQuery('''
          CREATE TABLE games(
          	id INTEGER PRIMARY KEY AUTOINCREMENT,
          	tag VARCHAR
          );
        ''');

        db.rawQuery('''
        CREATE TABLE entities(
        	id INTEGER PRIMARY KEY, 
	        entity_type TEXT,
	        game_id INTEGER,
	        CONSTRAINT fk_game
	        	FOREIGN KEY (game_id)
	        	REFERENCES games(id)
        );
        ''');

        db.rawQuery('''
        CREATE TABLE components(
        	id INTEGER PRIMARY KEY AUTOINCREMENT, 
        	component_type TEXT, 
        	data TEXT,
        	entity_id INTEGER,
        	CONSTRAINT fk_entity
        	    FOREIGN KEY (entity_id)
        		REFERENCES entities(id)
        );
        ''');

        db.rawQuery('''
        INSERT INTO games VALUES (
        	0,
        	"New save"
        );
        ''');

        db.rawQuery('''
        INSERT INTO entities VALUES (
        	0,
        	"Player",
          0
        );
        ''');

        db.rawQuery('''
        INSERT INTO components VALUES (
        	0,
        	"Health",
        	'{"maxValue": 100.0, "value": 100.0}',
        	0
        );''');

        db.rawQuery('''
        INSERT INTO components VALUES (
        	1,
        	"Position",
        	'{"x": 0.0, "y": 0.0}',
        	0
        );''');

        db.rawQuery('''
        INSERT INTO components VALUES (
        	2,
        	"Sprite",
        	'{"state": "player_moving_up", "scale": 0.3}',
        	0
        );''');

        // db.rawQuery('''
        // INSERT INTO components VALUES (
        // 	1,
        // 	"Position",
        // 	'{"x": 0.0, "y": 0.0}',
        // 	0
        // );''');

        // db.rawQuery('''
        // INSERT INTO components VALUES (
        // 	2,
        // 	"Velocity",
        // 	"{"vx": 0,0, "vy":0.0, "speed": 0.1}",
        // 	0
        // );
        // ''');

        // db.rawQuery('''
        // INSERT INTO components VALUES (
        // 	3,
        // 	"Sprite",
        // 	"{"state": "player_moving_up",
        // 	  "pivotAlignmentX": 0.5,
        // 	  "pivotAlignmentY": 0.5
        // 	 }",
        // 	0
        // );

        // ''');
      },
      version: 1,
    );
  }

  @override
  Future<List<Map<String, dynamic>>> execute(String query) {
    return database.rawQuery(query);
  }
}
