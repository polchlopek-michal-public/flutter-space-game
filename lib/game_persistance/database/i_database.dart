abstract class IDatabase {
  Future<void> open();
  Future<List<Map<String, dynamic>>> execute(String query);
}
