import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';

abstract class IGamePersistance {
  Future<void> save({EntitiesManager entitiesManager,
      ComponentsManager componentsManager, int gameId = 0});
  Future<void> load({EntitiesManager entitiesManager,
      ComponentsManager componentsManager, int gameId = 0});
}
