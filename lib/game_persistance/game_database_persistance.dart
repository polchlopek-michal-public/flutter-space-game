import 'dart:convert';

import 'package:game/game_logic/game_engine/entity.dart';
import 'package:game/game_logic/from_database_component_factory.dart';
import 'package:meta/meta.dart';
import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_persistance/database/i_database.dart';
import 'package:game/game_persistance/i_game_persistance.dart';

class GameDatabasePersistance implements IGamePersistance {
  IDatabase database;
  GameDatabasePersistance({@required this.database});
  FromDatabaseComponentFactory fromDatabaseFactory =
      FromDatabaseComponentFactory();

  @override
  Future<void> load({
    EntitiesManager entitiesManager,
    ComponentsManager componentsManager,
    int gameId = 0,
  }) {
    Future<void> returnFuture = () async {
      List<Map<String, dynamic>> entities = await database
          .execute("SELECT * FROM entities WHERE game_id = $gameId;");
      entities.forEach((data) {
        // entitiesManager.idGenerator.reserve(data["id"]);
        entitiesManager.addOrUpdateEntity(
          Entity(
            id: data["id"],
            type: data["entity_type"],
          ),
        );
      });

      List<Map<String, dynamic>> components = await database.execute('''
          SELECT  components.id,
                  components.entity_id,
                  components.component_type,
                  components.data
          FROM components INNER JOIN entities 
            ON components.entity_id = entities.id 
          WHERE entities.game_id = $gameId;
          ''');
      components.forEach((data) {
        componentsManager.add(
          fromDatabaseFactory.create(data["component_type"],jsonDecode(data["data"])),
          data["entity_id"],
        );
      });
    }();

    return returnFuture;
  }

  @override
  Future<void> save({
    EntitiesManager entitiesManager,
    ComponentsManager componentsManager,
    int gameId = 0,
  }) {
    return Future<void>(() {});
  }
}
