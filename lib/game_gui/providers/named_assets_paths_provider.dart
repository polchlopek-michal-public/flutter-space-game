import 'package:yaml/yaml.dart';
import 'package:meta/meta.dart';
import 'package:flutter/services.dart' show rootBundle;

class ConfigNotLoaded implements Exception {}

class SpriteStateNotFound implements Exception {}

class AssetData {
  final String assetName;
  final String mediaPath;
  final int width;
  final int height;
  final double alignmentX;
  final double alignmentY;
  AssetData({
    @required this.assetName,
    @required this.mediaPath,
    @required this.width,
    @required this.height,
    @required this.alignmentX,
    @required this.alignmentY,
  });
}

/// Loads config yaml file of mapped assets (name:path) and allows to read them.
///
/// [path] - path to config file
/// [header] = header of yaml file (namem of array {name,path} objects).
///
/// Simple mapping config file (sprites.yaml):
/// ```yaml
/// textues:  # header
///   - name: "example_texture_name"
///     path: "path_to_example_texture.png"
/// ```
/// Uses ["name"] operator to get associated path to asset.
/// Throws [ConfigNotLoaded] when user want's to get asset before loading mapping config file.
class NamedAssetsPathsProvider {
  String path;
  String header;
  bool isLoaded = false;
  List<AssetData> _data = [];

  NamedAssetsPathsProvider(this.path, this.header);

  /// Loads values from mapping config file..
  ///
  /// Function loads mapping config file from [path]
  /// to member [_data].
  /// Function must be invoked before using [] operator
  /// to get asset path by it's name.
  ///
  /// Throws [ConfigNotLoaded] exception when asset path cant be loaded.
  Future<bool> load() async {
    try {
      isLoaded = true;
      // Load from yaml:
      dynamic yamlData = loadYaml(await rootBundle.loadString(path));
      for (dynamic asset in yamlData[header]) {
        _data.add(new AssetData(
          assetName: asset["name"],
          mediaPath: asset["path"],
          width: asset["width"],
          height: asset["height"],
          alignmentX: asset["alignmentX"],
          alignmentY: asset["alignmentY"],
        ));
      }
    } catch (e) {
      throw ConfigNotLoaded();
    }
    return true;
  }

  /// Operator is used to get asset path by it's name.
  // String operator [](String name) {
  //   if (!isLoaded) throw ConfigNotLoaded();

  //   String mediaPath;
  //   _data.forEach((element) {
  //     if (element.assetName == name) {
  //       mediaPath = element.mediaPath;
  //       return;
  //     }
  //   });

  //   if (mediaPath == null) {
  //     throw SpriteStateNotFound;
  //   }

  //   return mediaPath;
  // }

  AssetData getAssetData(String name) {
    if (!isLoaded) throw ConfigNotLoaded();

    AssetData data;
    _data.forEach((element) {
      if (element.assetName == name) {
        data = element;
        return;
      }
    });
    return data;
  }
}
