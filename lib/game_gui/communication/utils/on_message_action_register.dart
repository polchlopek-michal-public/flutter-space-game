import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;

class OnMessageActionNotRegistered implements Exception {
  String _message;
  OnMessageActionNotRegistered(String message) : _message = message;
  String what() {
    return _message;
  }
}

class OnMessageActionRegister {
  Map<Proto.Logic2GuiMessage_Paylaod, Function> __actions = {};

  OnMessageActionRegister register(
      Proto.Logic2GuiMessage_Paylaod which, Function(Proto.Logic2GuiMessage) action) {
    __actions[which] = action;
    return this;
  }

  Function action(Proto.Logic2GuiMessage_Paylaod which) {
    if (__actions.keys.contains(which) == false) {
      throw OnMessageActionNotRegistered("Action $which is unregistered!");
    } else {
      return __actions[which];
    }
  }
}
