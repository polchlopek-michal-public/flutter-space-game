import 'dart:async';
import 'dart:typed_data';
import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:meta/meta.dart';

import 'package:game/game_communication/game_communication.dart';

class OfflineLogicCommunication implements GameCommunication {
  Stream<Uint8List> rawMessagesStream;
  StreamSink<Uint8List> rawMessagesSink;
  Function recvHandler;

  OfflineLogicCommunication({
    @required this.rawMessagesSink,
    @required this.rawMessagesStream,
  }) {
    rawMessagesStream.listen((Uint8List message) {
      recv(Proto.Message.fromBuffer(message));
    });
  }

  @override
  void send(Proto.Message message) {
    rawMessagesSink.add(message.writeToBuffer());
  }

  @override
  void recv(Proto.Message message) {
    recvHandler(message);
  }

  @override
  void setOnRecvHandler(Function handler) {
    recvHandler = handler;
  }
}
