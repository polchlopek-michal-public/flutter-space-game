import 'package:flutter/services.dart';
import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_gui/widgets/raised_gradient_button.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:game/game_communication/game_communication.dart';

class HomeScreen extends StatefulWidget {
  GameCommunication logicCommunication;
  HomeScreen({
    Key key,
    this.title = "",
    @required this.logicCommunication,
  }) : super(key: key);

  final String title;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage("assets/images/backgrounds/stars.jpg"),
          ),
        ),
        child: Container(
          child: ListView(
            children: [
              Container(
                padding: EdgeInsets.only(
                  top: 100,
                  bottom: 50,
                  left: 30,
                  right: 30,
                ),
                child: Image.asset(
                  'assets/images/moon_alone.png',
                  fit: BoxFit.scaleDown,
                ),
              ),
              Column(
                children: [
                  __buildPLayerNameInput(context: context),
                  SizedBox(height: 50),
                  __buildButton(
                    context: context,
                    text: "NEW GAME",
                    onPress: __onPressNewGameHandler,
                  ),
                  __buildButton(
                    context: context,
                    text: "LOAD GAME",
                    onPress: __onPressLoadGameHandler,
                  ),
                  __buildButton(
                    context: context,
                    text: "PLAY ONLINE",
                    onPress: () {},
                  ),
                  __buildButton(
                    context: context,
                    text: "QUIT GAME",
                    onPress: () {},
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget __buildButton({
    @required BuildContext context,
    @required String text,
    @required Function onPress,
  }) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.5,
      padding: EdgeInsets.all(10),
      child: RaisedGradientButton(
        child: Text(
          text,
          style: TextStyle(color: Colors.white),
        ),
        borderRadius: BorderRadius.circular(60.0),
        gradient: LinearGradient(
          colors: <Color>[
            Theme.of(context).accentColor,
            Theme.of(context).primaryColor
          ],
        ),
        onPressed: onPress,
      ),
    );
  }

  Widget __buildPLayerNameInput({
    @required BuildContext context,
  }) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.5,
      child: TextField(
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50),
          ),
          filled: true,
          fillColor: Theme.of(context).primaryColor.withAlpha(100),
          focusColor: Theme.of(context).primaryColor,
        ),
        style: TextStyle(
          color: Colors.white,
          shadows: [
            BoxShadow(
              color: Theme.of(context).accentColor,
              offset: Offset(0.5, 1.5),
              blurRadius: 1.5,
            ),
          ],
        ),
      ),
    );
  }

  __onPressNewGameHandler() {
    widget.logicCommunication.send(() {
      Proto.Message message = Proto.Message();
      message.gui2LoicMessage = Proto.Gui2LoicMessage();
      message.gui2LoicMessage.offlineGameStartedRequest =
          Proto.OfflineGameStartedRequest()..gameId = 0;
      return message;
    }());
  }

    __onPressLoadGameHandler() {
    widget.logicCommunication.send(() {
      Proto.Message message = Proto.Message();
      message.gui2LoicMessage = Proto.Gui2LoicMessage();
      message.gui2LoicMessage.offlineGameStartedRequest =
          Proto.OfflineGameStartedRequest()..gameId = 0;
      return message;
    }());
  }
}
