import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_communication/game_communication.dart';
import 'package:game/game_gui/camera/camera.dart';
import 'package:game/game_gui/screens/game_area_screen/game_area_screens_widgets/sprite_widget.dart';
import 'package:game/game_gui/types/gui_render_pack_types.dart';
import 'package:game/game_gui/providers/named_assets_paths_provider.dart';
import 'package:game/game_gui/screens/game_area_screen/game_area_screens_widgets/action_button_widget/action_button_widget.dart';
import 'package:game/game_gui/screens/game_area_screen/game_area_screens_widgets/game_pad/game_pad.dart';
import 'package:game/game_gui/screens/game_area_screen/render_bloc.dart';

class GameAreaScreen extends StatefulWidget {
  static String route = "game-area/";
  Camera camera;
  RenderBloc renderBloc;
  GameCommunication logicCommunication;

  GameAreaScreen({
    Key key,
    @required this.renderBloc,
    @required this.camera,
    @required this.logicCommunication,
  }) : super(key: key);

  @override
  _GameAreaScreenState createState() => _GameAreaScreenState();
}

class _GameAreaScreenState extends State<GameAreaScreen> {
  final NamedAssetsPathsProvider spriteStateProvider =
      NamedAssetsPathsProvider("assets/config/sprites.yaml", "sprites");
  Future<void> providersInitializedFuture;

  @override
  void initState() {
    _asyncInit();
    super.initState();
  }

  _asyncInit() {
    providersInitializedFuture = () async {
      await spriteStateProvider.load();
    }();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: providersInitializedFuture,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Container(
                color: Colors.cyan,
                child: Stack(children: <Widget>[
                  // StreamBuilder(
                  //     stream: widget.biomesBloc.biomes,
                  //     builder: (BuildContext context,
                  //         AsyncSnapshot<GuiBiomes> snapshot) {
                  //       // Real content:
                  //       if (snapshot == null) {
                  //         //TODO: handle...
                  //       } else {
                  //         return Container(
                  //           width: MediaQuery.of(context).size.width,
                  //           height: MediaQuery.of(context).size.height,
                  //           child: CustomPaint(
                  //             painter: BiomesPainter(
                  //                 context: context,
                  //                 biomes: snapshot.data,
                  //                 biomesPainterDependencies:
                  //                     widget.biomePainterDependecies,
                  //                 texturesProvider: texturesPathsProvider),
                  //           ),
                  //         );
                  //       }
                  //     }),

                  StreamBuilder(
                      stream: widget.renderBloc.renderPack,
                      builder: (BuildContext context,
                          AsyncSnapshot<GuiRenderPack> snapshot) {
                        if (snapshot == null || !snapshot.hasData) {
                          return Container();
                        } else {
                          return Stack(
                            children: snapshot.data.elements
                                .map((GuiRenderPackElement e) {
                              return SpriteWidget(
                                renderPackElement: e,
                                spriteStateProvider: spriteStateProvider,
                              );
                            }).toList(),
                          );
                        }
                      }),

                  // GamePad:
                  Positioned(
                    child: GamePad(
                      logicCommunication: widget.logicCommunication,
                    ),
                    left: 10,
                    bottom: 10,
                  ),

                  // GamePad:
                  Positioned(
                    child: ActionButtonWidget(
                      size: 50,
                      onPress: __actionButtonPressedHandler,
                      onRelease: __actionButtonReleasedHandler,
                    ),
                    right: 10,
                    bottom: 30,
                  ),
                ]));
          } else {
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }

  __actionButtonPressedHandler() {
    Proto.Message message = Proto.Message();
    message.gui2LoicMessage = Proto.Gui2LoicMessage();
    message.gui2LoicMessage.actionStart = Proto.ActionStart();
    widget.logicCommunication.send(message);
  }

  __actionButtonReleasedHandler() {
    Proto.Message message = Proto.Message();
    message.gui2LoicMessage = Proto.Gui2LoicMessage();
    message.gui2LoicMessage.actionStop = Proto.ActionStop();
    widget.logicCommunication.send(message);
  }
}
