import 'dart:math';

import 'package:flutter/material.dart';
import 'package:game/game_gui/screens/game_area_screen/game_area_screens_widgets/action_button_widget/arrow_painter.dart';

class ActionButtonWidget extends StatefulWidget {
  const ActionButtonWidget(
      {Key key,
      this.size = 50,
      this.arrowStroke = 5,
      this.outerColor = Colors.grey,
      this.innerColor = Colors.white,
      this.onPress,
      this.onRelease})
      : super(key: key);

  final double size;
  final double arrowStroke;
  final Color innerColor;
  final Color outerColor;

  final Function onPress;
  final Function onRelease;

  @override
  _ActionButtonWidgetState createState() => _ActionButtonWidgetState();
}

class _ActionButtonWidgetState extends State<ActionButtonWidget>
    with TickerProviderStateMixin {
  AnimationController _rectangleAnimationController;
  Animation<double> _rectangleAnimation;
  AnimationController _arrowsAnimationController;
  Animation<double> _arrowsAnimation;

  bool isActive = false;

  @override
  void initState() {
    super.initState();
    // _activeAnimationController =
    //     AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _rectangleAnimationController =
        AnimationController(vsync: this, duration: const Duration(seconds: 20));
    _rectangleAnimation = Tween<double>(begin: 0.0, end: (pi * 2)).animate(
        CurvedAnimation(
            parent: _rectangleAnimationController, curve: Curves.linear));

    _rectangleAnimation.addListener(() => setState(() {
          if (_rectangleAnimationController.isCompleted) {
            _rectangleAnimationController.value = 0;
            _rectangleAnimationController.forward();
          }
        }));

    setState(() {
      _rectangleAnimationController.forward();
    });

    _arrowsAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    _arrowsAnimation = Tween<double>(begin: 0.0, end: 10.0).animate(
        CurvedAnimation(
            parent: _arrowsAnimationController, curve: Curves.linear));

    _arrowsAnimationController.addListener(() => setState(() {
          if (_arrowsAnimationController.isCompleted && isActive) {
            _arrowsAnimationController.value = 0;
            _arrowsAnimationController.forward();
          }
        }));
  }

  @override
  void dispose() {
    _arrowsAnimationController.dispose();
    _rectangleAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanStart: (_) => _onPress(),
      onPanEnd: (_) => _onRelease(),
      child: Container(
        child: Row(children: [
          // Left arrows:
          buildLeftOutsideArrow(context), // inside arrow
          buildArrowPaint(context), // inside arrow
          buildRectangle(context),
          buildRightInsideArrow(context),
          buildRightOutsideArrow(context),
        ]),
      ),
    );
  }

  Widget buildArrowPaint(BuildContext context) {
    return Transform(
        transform: Matrix4(
            1,
            0,
            0,
            0, // --
            0,
            1,
            0,
            0, // --
            0,
            0,
            1,
            0, //-
            _arrowsAnimation.value - 10,
            0,
            0,
            1),
        child: CustomPaint(
            painter: ArrowPainter(
              strokeWidth: widget.arrowStroke,
              innerColor: widget.innerColor,
              outerColor: widget.outerColor,
            ),
            size: Size(widget.size / 2, widget.size)));
  }

  Widget buildLeftInsideArrow(BuildContext context) {
    return buildArrowPaint(context);
  }

  Widget buildLeftOutsideArrow(BuildContext context) {
    return Transform(
        transform: Matrix4(
            1,
            0,
            0,
            0, // --
            0,
            1,
            0,
            0, // --
            0,
            0,
            1,
            0, //-
            15,
            widget.size / 5,
            0,
            1 / 0.75),
        child: buildArrowPaint(context));
  }

  Widget buildRightInsideArrow(BuildContext context) {
    // TODO: make independent from left inside (own matrix4 transformation)
    return Transform.rotate(angle: pi, child: buildLeftInsideArrow(context));
  }

  Widget buildRightOutsideArrow(BuildContext context) {
    // TODO: make independent from left outside (own matrix4 transformation)
    return Transform.rotate(angle: pi, child: buildLeftOutsideArrow(context));
  }

  Widget buildRectangle(BuildContext context) {
    return Transform.rotate(
      angle: _rectangleAnimation.value,
      child: ClipRRect(
          child: Container(
            width: widget.size / 2,
            height: widget.size / 2,
            decoration: BoxDecoration(
              border: Border.all(color: widget.outerColor, width: 2),
              color: widget.innerColor,
            ),
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(0),
          )),
    );
  }

  _onPress() {
    if (!isActive) {
      setState(() {
        _arrowsAnimationController.forward();
      });
    }
    isActive = true;

    widget.onPress();
  }

  _onRelease() {
    isActive = false;
    _arrowsAnimationController.stop();
    widget.onRelease();
  }
}
