import 'package:flutter/material.dart';

class ArrowPainter extends CustomPainter {
  ArrowPainter({
    this.strokeWidth = 5,
    this.innerColor = Colors.white,
    this.outerColor = Colors.grey,
  });
  final double strokeWidth;
  final Color innerColor;
  final Color outerColor;

  @override
  void paint(Canvas canvas, Size size) {
    Path path = new Path();
    path.moveTo(size.width, size.height);
    path.lineTo(0, size.height / 2);
    path.lineTo(size.width, 0);
    Paint paint = new Paint()
      ..color = outerColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;
    canvas.drawPath(path, paint);

    Path pathInner = new Path();
    pathInner.moveTo(size.width, size.height);
    pathInner.lineTo(0, size.height / 2);
    pathInner.lineTo(size.width, 0);
    Paint paint2 = new Paint()
      ..color = innerColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth / 3;

    canvas.drawPath(pathInner, paint2);
  }

  @override
  bool shouldRepaint(ArrowPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(ArrowPainter oldDelegate) => false;
}
