import 'package:flutter/rendering.dart';
import 'package:flutter/painting.dart';

class FingerPrintPainter extends CustomPainter {
  double __radius = 80;
  bool __isFingerDown = false;
  double __x = 0;
  double __y = 0;

  Color fingerPrintColor = Color.fromRGBO(255, 255, 255, 0);
  final double maxOpacity = 0.3;
  double opacity = 0;

  FingerPrintPainter() {
    opacity = maxOpacity;
    fingerPrintColor = fingerPrintColor.withOpacity(opacity);
  }

  @override
  void paint(Canvas canvas, Size size) {
    if (__isFingerDown) {
      Paint paint = Paint();
      paint.color = fingerPrintColor;
      canvas.drawCircle(Offset(__x, __y), __radius, paint);
      canvas.restore();
      canvas.save();
    }
  }

  fingerUp() async {
    while (opacity > 0) {
      await Future.delayed(Duration(milliseconds: 30));
      try {
        opacity = opacity - maxOpacity / 10;
      } catch (e) {
        opacity = 0;
      }
      opacity = opacity < 0 ? 0 : opacity;
      fingerPrintColor = fingerPrintColor.withOpacity(opacity);
    }
    __isFingerDown = false;
    opacity = maxOpacity;
    fingerPrintColor = fingerPrintColor.withOpacity(opacity);
  }

  fingerDown(double x, double y) {
    __x = x;
    __y = y;
    __isFingerDown = true;
  }

  @override
  bool shouldRepaint(FingerPrintPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(FingerPrintPainter oldDelegate) => false;
}
