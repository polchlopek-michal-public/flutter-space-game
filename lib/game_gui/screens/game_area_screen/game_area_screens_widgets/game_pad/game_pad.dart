import 'package:flutter/widgets.dart';
import 'package:game/game_communication/game_communication.dart';
import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_gui/screens/game_area_screen/game_area_screens_widgets/game_pad/finger_print-painter.dart';
import 'dart:math';
import 'package:tuple/tuple.dart';

class GamePad extends StatefulWidget {
  GamePad({Key key, @required this.logicCommunication}) : super(key: key);
  GameCommunication logicCommunication;

  @override
  _GamePadState createState() => _GamePadState();
}

class _GamePadState extends State<GamePad> {
  Color fillColor = Color.fromRGBO(255, 255, 255, 0.6);
  double size = 200;
  Color borderColor = Color.fromRGBO(0, 0, 0, 0.4);
  double borderWidth = 5;

  double calculateAngle(
      Tuple2<double, double> tapPoint, Tuple2<double, double> centerPoint) {
    Tuple2<double, double> i = Tuple2<double, double>(-1.0, 0);
    Tuple2<double, double> v = Tuple2<double, double>(
        tapPoint.item1 - centerPoint.item1, tapPoint.item2 - centerPoint.item2);

    double vValue = sqrt(v.item1 * v.item1 + v.item2 * v.item2);
    double iValue = sqrt(i.item1 * i.item1 + i.item2 * i.item2);
    double scalarValue = v.item1 * i.item1 + v.item2 * i.item2;

    double cosinus = scalarValue / (vValue * iValue);
    double retVal = acos(cosinus);
    retVal = tapPoint.item2 < centerPoint.item2 ? 6.2830 - retVal : retVal;
    return retVal;
  }

  double calculateProcentageRange(Tuple2<double, double> centerPoint,
      Tuple2<double, double> tapPoint, double radius) {
    Tuple2<double, double> v = Tuple2<double, double>(
        tapPoint.item1 - centerPoint.item1, tapPoint.item2 - centerPoint.item2);

    double vVal = sqrt(v.item1 * v.item1 + v.item2 * v.item2);

    double range = vVal / radius;
    range = range > 1 ? 1 : range;
    return range;
  }

  dynamic onPadUpdate(dynamic details) {
    fingerPrintPainter.fingerDown(
        details.localPosition.dx, details.localPosition.dy);
    Proto.Message message = Proto.Message();
    message.gui2LoicMessage = Proto.Gui2LoicMessage();
    message.gui2LoicMessage.inputMove = Proto.InputMove();
    message.gui2LoicMessage.inputMove.directionAngle = calculateAngle(
        Tuple2<double, double>(
            details.localPosition.dx, -details.localPosition.dy),
        Tuple2<double, double>(size / 2, -size / 2));
    message.gui2LoicMessage.inputMove.speed = calculateProcentageRange(
        Tuple2<double, double>(size / 2, -size / 2),
        Tuple2<double, double>(
            details.localPosition.dx, -details.localPosition.dy),
        size / 2);
    widget.logicCommunication.send(message);
  }

  FingerPrintPainter fingerPrintPainter = FingerPrintPainter();

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: GestureDetector(
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(color: borderColor, width: borderWidth),
              borderRadius: BorderRadius.all(
                Radius.circular(100),
              ),
              color: fillColor),
          width: size,
          height: size,
          child: CustomPaint(painter: fingerPrintPainter),
        ),
        onPanDown: onPadUpdate,
        onPanUpdate: onPadUpdate,
        onPanEnd: (_) {
          Proto.Message message = Proto.Message();
          message.gui2LoicMessage = Proto.Gui2LoicMessage();
          message.gui2LoicMessage.inputStopMove = Proto.InputStopMove();
          widget.logicCommunication.send(message);
          fingerPrintPainter.fingerUp();
        },
      ),
    );
  }
}
