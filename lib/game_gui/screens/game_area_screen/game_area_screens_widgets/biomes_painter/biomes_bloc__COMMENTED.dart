// import 'dart:async';
// import 'package:basicloop/gui/game_map/gui_biomes.dart';

// // Bloc events:

// abstract class BiomesBlocEvent {}

// class InitBiomesBlocEvent extends BiomesBlocEvent {
//   GuiBiomes biomes = GuiBiomes();
//   InitBiomesBlocEvent(this.biomes);
// }

// class TranslateBiomesShapeBiomesBlocEvent extends BiomesBlocEvent {
//   GuiBiomes biomes = GuiBiomes();
//   TranslateBiomesShapeBiomesBlocEvent(this.biomes);
// }

// // Bloc:
// class BiomesBloc {
//   GuiBiomes guiBiomes = GuiBiomes();

//   final _biomesStateController = StreamController<GuiBiomes>.broadcast();
//   final _biomesEventController = StreamController<BiomesBlocEvent>();

//   Stream<GuiBiomes> get biomes => _biomesStateController.stream;
//   StreamSink<BiomesBlocEvent> get biomesEventsSink =>
//       _biomesEventController.sink;

//   BiomesBloc() {
//     _biomesEventController.stream.listen(onEvent);
//     _biomesStateController.sink.add(guiBiomes);
//   }

//   /// Gets initialized biomes.
//   GuiBiomes getBiomes() => guiBiomes;

//   /// Handler of incomming events. Sends biomes to stream.
//   void onEvent(BiomesBlocEvent event) {
//     if (event is InitBiomesBlocEvent) {
//       guiBiomes = event.biomes;
//       _biomesStateController.sink.add(guiBiomes);
//     } else if (event is TranslateBiomesShapeBiomesBlocEvent) {
//       _biomesStateController.sink.add(event.biomes);
//     }
//   }
// }
