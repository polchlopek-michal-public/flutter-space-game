// import 'dart:ui' as Ui;

// import 'package:basicloop/types/biome_pack.dart';
// import 'package:basicloop/types/chunk.dart';
// import 'package:flutter/services.dart';
// import 'package:meta/meta.dart';
// import 'package:basicloop/gui/game_map/gui_biomes.dart';
// import 'package:basicloop/providers/named_assets_paths_provider.dart';
// import 'package:flutter/material.dart';

// class BiomesPainterDependencies {
//   Map<String, Ui.Image> images = {};
// }

// class BiomesPainter extends CustomPainter {
//   final GuiBiomes biomes;
//   final NamedAssetsPathsProvider texturesProvider;
//   final BuildContext context;
//   final BiomesPainterDependencies biomesPainterDependencies;

//   BiomesPainter(
//       {@required this.biomes,
//       @required this.texturesProvider,
//       @required this.biomesPainterDependencies,
//       @required this.context});

//   @override
//   void paint(Canvas canvas, Size size) {
//     Paint paint = new Paint()
//       ..color = Colors.black
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = 4.0;

//     biomes.biomes.forEach((BiomePack biomePack) {
//       if (!biomesPainterDependencies.images
//           .containsKey(biomePack.textureName)) {
//         _loadTextureImage(biomePack.textureName).then((Ui.Image value) {
//           biomesPainterDependencies.images["${biomePack.textureName}"] = value;
//         });
//       }

//       biomePack.chunks.forEach((Chunk chunk) {
//         if (biomesPainterDependencies.images
//             .containsKey(biomePack.textureName)) {
//           canvas.drawImage(
//               biomesPainterDependencies.images[biomePack.textureName],
//               Ui.Offset(
//                   _mapXCoordinate(chunk.ul.x), _mapYCoordinate(chunk.ul.y)),
//               paint);
//         } else {
//           // TODO: handle...
//         }
//       });
//     });
//   }

//   @override
//   bool shouldRepaint(BiomesPainter oldDelegate) => true;

//   double _mapXCoordinate(double x) {
//     return MediaQuery.of(context).size.width / 2 + x;
//   }

//   double _mapYCoordinate(double y) {
//     return MediaQuery.of(context).size.height / 2 - y;
//   }

//   Future<Ui.Image> _loadTextureImage(final String textureName) async {
//     final data = await rootBundle.load(texturesProvider[textureName]);
//     return decodeImageFromList(data.buffer.asUint8List());
//   }
// }
