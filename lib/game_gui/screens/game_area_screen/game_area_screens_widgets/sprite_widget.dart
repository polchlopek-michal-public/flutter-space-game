import 'package:flutter/material.dart';
import 'package:game/game_gui/providers/named_assets_paths_provider.dart';
import 'package:game/game_gui/types/gui_render_pack_types.dart';

class SpriteWidget extends StatelessWidget {
  final NamedAssetsPathsProvider spriteStateProvider;
  final GuiRenderPackElement renderPackElement;
  final debugMode = true;

  SpriteWidget(
      {@required this.spriteStateProvider, @required this.renderPackElement});

  @override
  Widget build(BuildContext context) {
    double left = MediaQuery.of(context).size.width / 2 + renderPackElement.x;
    double top = MediaQuery.of(context).size.height / 2 - renderPackElement.y;
    AssetData spriteAssetData =
        spriteStateProvider.getAssetData(renderPackElement.state);
    return Positioned(
      left: left,
      top: top,
      child: Stack(
        children: [
          Transform(
            transform: Matrix4(
                1,
                0,
                0,
                0,
                // ---
                0,
                1,
                0,
                0,
                // ---
                0,
                0,
                1,
                0,
                // ---
                    -((spriteAssetData.width) * spriteAssetData.alignmentX),
                -(spriteAssetData.height / 2) +
                    (spriteAssetData.height / 2) * spriteAssetData.alignmentY,
                0,
                1 / renderPackElement.scale),
            child: Image.asset(spriteAssetData.mediaPath),
          ),
          debugMode
              ? Column(
                  // left: left,
                  // top: top,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 3,
                      height: 3,
                      color: Colors.red,
                    ),
                    Text(
                        "${renderPackElement.x.floor()};${renderPackElement.y.floor()}")
                  ],
                )
              : Container(),
        ],
      ),
    );
  }
}
