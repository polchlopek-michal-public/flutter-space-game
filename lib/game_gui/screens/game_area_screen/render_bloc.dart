import 'dart:async';
import 'package:game/game_gui/types/gui_render_pack_types.dart';


// --------- events ---------------
abstract class RenderBlockEvent {}

class UpdateRenderPackEvent extends RenderBlockEvent {
  GuiRenderPack guiRenderPack;
  UpdateRenderPackEvent(this.guiRenderPack);
}

// ------------ bloc -------------

class RenderBloc {
  GuiRenderPack _guiRenderPack = GuiRenderPack();

  final _guiRenderPackStateController = StreamController<GuiRenderPack>();
  final _renderBlocEventsController = StreamController<RenderBlockEvent>();

  Stream<GuiRenderPack> get renderPack => _guiRenderPackStateController.stream;
  StreamSink<RenderBlockEvent> get renderPackSink =>
      _renderBlocEventsController.sink;

  RenderBloc() {
    _renderBlocEventsController.stream.listen(onEvent);
  }

  void onEvent(RenderBlockEvent event) {
    if (event is UpdateRenderPackEvent) {
      _guiRenderPack = event.guiRenderPack;
      _guiRenderPackStateController.sink.add(_guiRenderPack);
    }
  }
}
