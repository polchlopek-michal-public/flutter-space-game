import 'package:game/game_gui/types/gui_render_pack_types.dart';

class TargetNotFoundInRenderPack implements Exception {}

class Camera {
  int _targetId;
  double _targetXPos;
  double _targetYPos;

  Camera();

  void attach(int targetId) {
    _targetId = targetId;
  }

  GuiRenderPack makePositionDependentToTarget(GuiRenderPack renderPack) {
    bool targetFound = false;
    renderPack.elements.forEach((GuiRenderPackElement element) {
      if (element.entityId == _targetId) {
        targetFound = true;
        _targetXPos = element.x;
        _targetYPos = element.y;
      }
    });

    if (!targetFound) {
      throw TargetNotFoundInRenderPack();
    }

    //recalculating all objects:
    renderPack.elements.forEach((GuiRenderPackElement object) {
      object.x -= _targetXPos;
      object.y -= _targetYPos;
    });

    // recalculating biomes:
    // GuiBiomes newBiomes = GuiBiomes.copy(biomesBloc.guiBiomes);
    // newBiomes.biomes.forEach((BiomePack biomes) {
    //   biomes.chunks.forEach((Chunk chunk) {
    //     chunk.ul.x -= _targetXPos;
    //     chunk.dr.x -= _targetXPos;

    //     chunk.ul.y -= _targetYPos;
    //     chunk.dr.y -= _targetYPos;
    //   });
    // });
    // biomesBloc.onEvent(TranslateBiomesShapeBiomesBlocEvent(newBiomes));

    return renderPack;
  }

  // GuiBiomes makeBiomeShapeDependentToTarget(GuiBiomes biomes) {
  //   biomes.biomes.forEach((BiomePack biome) {
  //     biome.points.forEach((Point point) {
  //       point.x -= _targetXPos;
  //       point.y -= _targetYPos;
  //     });
  //   });
  //   return biomes;
  // }
}
