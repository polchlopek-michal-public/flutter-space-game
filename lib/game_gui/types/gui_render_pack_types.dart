import 'package:meta/meta.dart';
import 'package:game/game_communication/generated/lib/game_communication/messages.pbserver.dart' as Proto;

class GuiRenderPackElement {
  int entityId;
  double x;
  double y;
  String state;
  double scale;

  GuiRenderPackElement({
    @required this.x,
    @required this.y,
    @required this.state,
    @required this.scale,
  });
}
 
class GuiRenderPack {
  List<GuiRenderPackElement> elements = [];
  GuiRenderPack({this.elements});
  
  GuiRenderPack.fromProto(Proto.Logic2GuiMessage message) {
    elements = [];
    message.renderPack.elements.forEach((Proto.RenderPackElement protoRenderPackElement) { 
      elements.add(GuiRenderPackElement(
        x: protoRenderPackElement.position.x,
        y: protoRenderPackElement.position.y,
        state: protoRenderPackElement.sprite.state,
        scale: protoRenderPackElement.sprite.scale,
      ));
    });
  }
}
