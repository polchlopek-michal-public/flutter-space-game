import 'package:flutter/material.dart';
import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_gui/camera/camera.dart';
import 'package:game/game_gui/communication/offline_logic_communication.dart';
import 'package:game/game_gui/communication/utils/on_message_action_register.dart';
import 'package:game/game_gui/screens/game_area_screen/game_area_screen.dart';
import 'package:game/game_gui/screens/game_area_screen/render_bloc.dart';
import 'package:game/game_gui/screens/home_screen/home_screen.dart';
import 'package:game/game_gui/types/gui_render_pack_types.dart';

class GameApp extends StatelessWidget {
  OfflineLogicCommunication logicCommunication;
  OnMessageActionRegister onMessageActionRegister;
  RenderBloc renderBloc = RenderBloc();
  Camera camera = new Camera();
  BuildContext _context;

  // --------- registering messages actions --------
  GameApp({this.logicCommunication}) {
    onMessageActionRegister = OnMessageActionRegister()
        // OfflineGameStarted Message:
        .register(Proto.Logic2GuiMessage_Paylaod.offlineGameStarted,
            (Proto.Logic2GuiMessage message) {
      Navigator.of(_context).pushNamed(GameAreaScreen.route);
      print("Game started");
      // RenderPack Message:
    }).register(Proto.Logic2GuiMessage_Paylaod.renderPack,
            (Proto.Logic2GuiMessage message) {
      renderBloc
          .onEvent(UpdateRenderPackEvent(GuiRenderPack.fromProto(message)));
      // AttachCameraTarget Message:
    }).register(Proto.Logic2GuiMessage_Paylaod.attachCameraTarget,
            (Proto.Logic2GuiMessage message) {
      camera.attach(message.attachCameraTarget.targetId);
      print(
          "Camera attached on target {target id:${message.attachCameraTarget.targetId}}");
    });

    // ------------- seting action handler ----------------
    logicCommunication.setOnRecvHandler((Proto.Message message) {
      onMessageActionRegister.action(message.logic2GuiMessage.whichPaylaod())(
          message.logic2GuiMessage);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Color(0xFFB06EB9),
        accentColor: Color(0xFFCA0000),
        fontFamily: 'Montserrat',
      ),
      home: Builder(
        builder: (context) {
          _context = context;
          return HomeScreen(
            title: 'Moon Alone',
            logicCommunication: logicCommunication,
          );
        },
      ),
      routes: {
        GameAreaScreen.route: (BuildContext context) => GameAreaScreen(
              renderBloc: renderBloc,
              logicCommunication: logicCommunication,
              camera: camera,
            ),
      },
    );
  }
}
