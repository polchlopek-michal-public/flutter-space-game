class UnknownApplicationMode implements Exception {
  String _message;
  UnknownApplicationMode(String message) : _message = message;
  String what() {
    return _message;
  }
}

enum ApplicationMode {
  OFFLINE,
  ONLINE,
}
