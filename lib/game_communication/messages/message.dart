abstract class Message {

  /// Create Message using serialized message. (default - using Proto)
  Message.fromProto(dynamic strMessage);

  /// Convert to serialzied Proto message. (default - using Proto)
  String serialzie();
}
