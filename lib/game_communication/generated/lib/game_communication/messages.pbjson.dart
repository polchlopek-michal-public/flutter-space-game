///
//  Generated code. Do not modify.
//  source: lib/game_communication/messages.proto
//
// @dart = 2.3
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

const Message$json = const {
  '1': 'Message',
  '2': const [
    const {'1': 'logic2GuiMessage', '3': 1, '4': 1, '5': 11, '6': '.Logic2GuiMessage', '9': 0, '10': 'logic2GuiMessage'},
    const {'1': 'gui2LoicMessage', '3': 2, '4': 1, '5': 11, '6': '.Gui2LoicMessage', '9': 0, '10': 'gui2LoicMessage'},
  ],
  '8': const [
    const {'1': 'paylaod'},
  ],
};

const Logic2GuiMessage$json = const {
  '1': 'Logic2GuiMessage',
  '2': const [
    const {'1': 'renderPack', '3': 1, '4': 1, '5': 11, '6': '.RenderPack', '9': 0, '10': 'renderPack'},
    const {'1': 'playerJoinRejected', '3': 2, '4': 1, '5': 11, '6': '.PlayerJoinRejected', '9': 0, '10': 'playerJoinRejected'},
    const {'1': 'offlineGameStarted', '3': 3, '4': 1, '5': 11, '6': '.OfflineGameStarted', '9': 0, '10': 'offlineGameStarted'},
    const {'1': 'savedGamesResponse', '3': 4, '4': 1, '5': 11, '6': '.SavedGamesResponse', '9': 0, '10': 'savedGamesResponse'},
    const {'1': 'attachCameraTarget', '3': 5, '4': 1, '5': 11, '6': '.AttachCameraTarget', '9': 0, '10': 'attachCameraTarget'},
  ],
  '8': const [
    const {'1': 'paylaod'},
  ],
};

const RenderPack$json = const {
  '1': 'RenderPack',
  '2': const [
    const {'1': 'elements', '3': 1, '4': 3, '5': 11, '6': '.RenderPackElement', '10': 'elements'},
  ],
};

const PlayerJoinRejected$json = const {
  '1': 'PlayerJoinRejected',
};

const OfflineGameStarted$json = const {
  '1': 'OfflineGameStarted',
};

const SavedGame$json = const {
  '1': 'SavedGame',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 13, '10': 'id'},
    const {'1': 'tag', '3': 2, '4': 1, '5': 9, '10': 'tag'},
  ],
};

const SavedGamesResponse$json = const {
  '1': 'SavedGamesResponse',
  '2': const [
    const {'1': 'savedGames', '3': 1, '4': 3, '5': 11, '6': '.SavedGame', '10': 'savedGames'},
  ],
};

const AttachCameraTarget$json = const {
  '1': 'AttachCameraTarget',
  '2': const [
    const {'1': 'targetId', '3': 1, '4': 1, '5': 13, '10': 'targetId'},
  ],
};

const Gui2LoicMessage$json = const {
  '1': 'Gui2LoicMessage',
  '2': const [
    const {'1': 'playerId', '3': 1, '4': 1, '5': 9, '10': 'playerId'},
    const {'1': 'playerJoined', '3': 2, '4': 1, '5': 11, '6': '.PlayerJoined', '9': 0, '10': 'playerJoined'},
    const {'1': 'inputMove', '3': 3, '4': 1, '5': 11, '6': '.InputMove', '9': 0, '10': 'inputMove'},
    const {'1': 'inputStopMove', '3': 4, '4': 1, '5': 11, '6': '.InputStopMove', '9': 0, '10': 'inputStopMove'},
    const {'1': 'actionStart', '3': 5, '4': 1, '5': 11, '6': '.ActionStart', '9': 0, '10': 'actionStart'},
    const {'1': 'actionStop', '3': 6, '4': 1, '5': 11, '6': '.ActionStop', '9': 0, '10': 'actionStop'},
    const {'1': 'offlineGameStartedRequest', '3': 7, '4': 1, '5': 11, '6': '.OfflineGameStartedRequest', '9': 0, '10': 'offlineGameStartedRequest'},
    const {'1': 'savedGamesRequest', '3': 8, '4': 1, '5': 11, '6': '.SavedGamesRequest', '9': 0, '10': 'savedGamesRequest'},
  ],
  '8': const [
    const {'1': 'paylaod'},
  ],
};

const PlayerJoined$json = const {
  '1': 'PlayerJoined',
  '2': const [
    const {'1': 'playerId', '3': 1, '4': 1, '5': 9, '10': 'playerId'},
  ],
};

const InputMove$json = const {
  '1': 'InputMove',
  '2': const [
    const {'1': 'directionAngle', '3': 1, '4': 1, '5': 1, '10': 'directionAngle'},
    const {'1': 'speed', '3': 2, '4': 1, '5': 1, '10': 'speed'},
  ],
};

const InputStopMove$json = const {
  '1': 'InputStopMove',
};

const ActionStart$json = const {
  '1': 'ActionStart',
};

const ActionStop$json = const {
  '1': 'ActionStop',
};

const OfflineGameStartedRequest$json = const {
  '1': 'OfflineGameStartedRequest',
  '2': const [
    const {'1': 'gameId', '3': 1, '4': 1, '5': 13, '10': 'gameId'},
    const {'1': 'playerName', '3': 2, '4': 1, '5': 9, '10': 'playerName'},
  ],
};

const SavedGamesRequest$json = const {
  '1': 'SavedGamesRequest',
};

const RenderPackElement$json = const {
  '1': 'RenderPackElement',
  '2': const [
    const {'1': 'entityId', '3': 1, '4': 1, '5': 13, '10': 'entityId'},
    const {'1': 'position', '3': 2, '4': 1, '5': 11, '6': '.Position', '10': 'position'},
    const {'1': 'sprite', '3': 3, '4': 1, '5': 11, '6': '.Sprite', '10': 'sprite'},
  ],
};

const Component$json = const {
  '1': 'Component',
  '2': const [
    const {'1': 'position', '3': 1, '4': 1, '5': 11, '6': '.Position', '9': 0, '10': 'position'},
    const {'1': 'velocity', '3': 2, '4': 1, '5': 11, '6': '.Velocity', '9': 0, '10': 'velocity'},
    const {'1': 'helath', '3': 3, '4': 1, '5': 11, '6': '.Health', '9': 0, '10': 'helath'},
    const {'1': 'sprite', '3': 4, '4': 1, '5': 11, '6': '.Sprite', '9': 0, '10': 'sprite'},
  ],
  '8': const [
    const {'1': 'component'},
  ],
};

const Position$json = const {
  '1': 'Position',
  '2': const [
    const {'1': 'x', '3': 1, '4': 1, '5': 1, '10': 'x'},
    const {'1': 'y', '3': 2, '4': 1, '5': 1, '10': 'y'},
  ],
};

const Velocity$json = const {
  '1': 'Velocity',
  '2': const [
    const {'1': 'maxValue', '3': 1, '4': 1, '5': 1, '10': 'maxValue'},
    const {'1': 'value', '3': 2, '4': 1, '5': 1, '10': 'value'},
  ],
};

const Health$json = const {
  '1': 'Health',
  '2': const [
    const {'1': 'maxValue', '3': 1, '4': 1, '5': 1, '10': 'maxValue'},
    const {'1': 'value', '3': 2, '4': 1, '5': 1, '10': 'value'},
  ],
};

const Sprite$json = const {
  '1': 'Sprite',
  '2': const [
    const {'1': 'state', '3': 1, '4': 1, '5': 9, '10': 'state'},
    const {'1': 'scale', '3': 2, '4': 1, '5': 1, '10': 'scale'},
  ],
};

