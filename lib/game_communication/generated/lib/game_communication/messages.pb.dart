///
//  Generated code. Do not modify.
//  source: lib/game_communication/messages.proto
//
// @dart = 2.3
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

enum Message_Paylaod {
  logic2GuiMessage, 
  gui2LoicMessage, 
  notSet
}

class Message extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Message_Paylaod> _Message_PaylaodByTag = {
    1 : Message_Paylaod.logic2GuiMessage,
    2 : Message_Paylaod.gui2LoicMessage,
    0 : Message_Paylaod.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Message', createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOM<Logic2GuiMessage>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'logic2GuiMessage', protoName: 'logic2GuiMessage', subBuilder: Logic2GuiMessage.create)
    ..aOM<Gui2LoicMessage>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gui2LoicMessage', protoName: 'gui2LoicMessage', subBuilder: Gui2LoicMessage.create)
    ..hasRequiredFields = false
  ;

  Message._() : super();
  factory Message() => create();
  factory Message.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Message.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Message clone() => Message()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Message copyWith(void Function(Message) updates) => super.copyWith((message) => updates(message as Message)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Message create() => Message._();
  Message createEmptyInstance() => create();
  static $pb.PbList<Message> createRepeated() => $pb.PbList<Message>();
  @$core.pragma('dart2js:noInline')
  static Message getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Message>(create);
  static Message _defaultInstance;

  Message_Paylaod whichPaylaod() => _Message_PaylaodByTag[$_whichOneof(0)];
  void clearPaylaod() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  Logic2GuiMessage get logic2GuiMessage => $_getN(0);
  @$pb.TagNumber(1)
  set logic2GuiMessage(Logic2GuiMessage v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasLogic2GuiMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearLogic2GuiMessage() => clearField(1);
  @$pb.TagNumber(1)
  Logic2GuiMessage ensureLogic2GuiMessage() => $_ensure(0);

  @$pb.TagNumber(2)
  Gui2LoicMessage get gui2LoicMessage => $_getN(1);
  @$pb.TagNumber(2)
  set gui2LoicMessage(Gui2LoicMessage v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasGui2LoicMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearGui2LoicMessage() => clearField(2);
  @$pb.TagNumber(2)
  Gui2LoicMessage ensureGui2LoicMessage() => $_ensure(1);
}

enum Logic2GuiMessage_Paylaod {
  renderPack, 
  playerJoinRejected, 
  offlineGameStarted, 
  savedGamesResponse, 
  attachCameraTarget, 
  notSet
}

class Logic2GuiMessage extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Logic2GuiMessage_Paylaod> _Logic2GuiMessage_PaylaodByTag = {
    1 : Logic2GuiMessage_Paylaod.renderPack,
    2 : Logic2GuiMessage_Paylaod.playerJoinRejected,
    3 : Logic2GuiMessage_Paylaod.offlineGameStarted,
    4 : Logic2GuiMessage_Paylaod.savedGamesResponse,
    5 : Logic2GuiMessage_Paylaod.attachCameraTarget,
    0 : Logic2GuiMessage_Paylaod.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Logic2GuiMessage', createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4, 5])
    ..aOM<RenderPack>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'renderPack', protoName: 'renderPack', subBuilder: RenderPack.create)
    ..aOM<PlayerJoinRejected>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'playerJoinRejected', protoName: 'playerJoinRejected', subBuilder: PlayerJoinRejected.create)
    ..aOM<OfflineGameStarted>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'offlineGameStarted', protoName: 'offlineGameStarted', subBuilder: OfflineGameStarted.create)
    ..aOM<SavedGamesResponse>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'savedGamesResponse', protoName: 'savedGamesResponse', subBuilder: SavedGamesResponse.create)
    ..aOM<AttachCameraTarget>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'attachCameraTarget', protoName: 'attachCameraTarget', subBuilder: AttachCameraTarget.create)
    ..hasRequiredFields = false
  ;

  Logic2GuiMessage._() : super();
  factory Logic2GuiMessage() => create();
  factory Logic2GuiMessage.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Logic2GuiMessage.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Logic2GuiMessage clone() => Logic2GuiMessage()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Logic2GuiMessage copyWith(void Function(Logic2GuiMessage) updates) => super.copyWith((message) => updates(message as Logic2GuiMessage)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Logic2GuiMessage create() => Logic2GuiMessage._();
  Logic2GuiMessage createEmptyInstance() => create();
  static $pb.PbList<Logic2GuiMessage> createRepeated() => $pb.PbList<Logic2GuiMessage>();
  @$core.pragma('dart2js:noInline')
  static Logic2GuiMessage getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Logic2GuiMessage>(create);
  static Logic2GuiMessage _defaultInstance;

  Logic2GuiMessage_Paylaod whichPaylaod() => _Logic2GuiMessage_PaylaodByTag[$_whichOneof(0)];
  void clearPaylaod() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  RenderPack get renderPack => $_getN(0);
  @$pb.TagNumber(1)
  set renderPack(RenderPack v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasRenderPack() => $_has(0);
  @$pb.TagNumber(1)
  void clearRenderPack() => clearField(1);
  @$pb.TagNumber(1)
  RenderPack ensureRenderPack() => $_ensure(0);

  @$pb.TagNumber(2)
  PlayerJoinRejected get playerJoinRejected => $_getN(1);
  @$pb.TagNumber(2)
  set playerJoinRejected(PlayerJoinRejected v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasPlayerJoinRejected() => $_has(1);
  @$pb.TagNumber(2)
  void clearPlayerJoinRejected() => clearField(2);
  @$pb.TagNumber(2)
  PlayerJoinRejected ensurePlayerJoinRejected() => $_ensure(1);

  @$pb.TagNumber(3)
  OfflineGameStarted get offlineGameStarted => $_getN(2);
  @$pb.TagNumber(3)
  set offlineGameStarted(OfflineGameStarted v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasOfflineGameStarted() => $_has(2);
  @$pb.TagNumber(3)
  void clearOfflineGameStarted() => clearField(3);
  @$pb.TagNumber(3)
  OfflineGameStarted ensureOfflineGameStarted() => $_ensure(2);

  @$pb.TagNumber(4)
  SavedGamesResponse get savedGamesResponse => $_getN(3);
  @$pb.TagNumber(4)
  set savedGamesResponse(SavedGamesResponse v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasSavedGamesResponse() => $_has(3);
  @$pb.TagNumber(4)
  void clearSavedGamesResponse() => clearField(4);
  @$pb.TagNumber(4)
  SavedGamesResponse ensureSavedGamesResponse() => $_ensure(3);

  @$pb.TagNumber(5)
  AttachCameraTarget get attachCameraTarget => $_getN(4);
  @$pb.TagNumber(5)
  set attachCameraTarget(AttachCameraTarget v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasAttachCameraTarget() => $_has(4);
  @$pb.TagNumber(5)
  void clearAttachCameraTarget() => clearField(5);
  @$pb.TagNumber(5)
  AttachCameraTarget ensureAttachCameraTarget() => $_ensure(4);
}

class RenderPack extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RenderPack', createEmptyInstance: create)
    ..pc<RenderPackElement>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'elements', $pb.PbFieldType.PM, subBuilder: RenderPackElement.create)
    ..hasRequiredFields = false
  ;

  RenderPack._() : super();
  factory RenderPack() => create();
  factory RenderPack.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RenderPack.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RenderPack clone() => RenderPack()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RenderPack copyWith(void Function(RenderPack) updates) => super.copyWith((message) => updates(message as RenderPack)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RenderPack create() => RenderPack._();
  RenderPack createEmptyInstance() => create();
  static $pb.PbList<RenderPack> createRepeated() => $pb.PbList<RenderPack>();
  @$core.pragma('dart2js:noInline')
  static RenderPack getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RenderPack>(create);
  static RenderPack _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<RenderPackElement> get elements => $_getList(0);
}

class PlayerJoinRejected extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PlayerJoinRejected', createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  PlayerJoinRejected._() : super();
  factory PlayerJoinRejected() => create();
  factory PlayerJoinRejected.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PlayerJoinRejected.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PlayerJoinRejected clone() => PlayerJoinRejected()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PlayerJoinRejected copyWith(void Function(PlayerJoinRejected) updates) => super.copyWith((message) => updates(message as PlayerJoinRejected)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PlayerJoinRejected create() => PlayerJoinRejected._();
  PlayerJoinRejected createEmptyInstance() => create();
  static $pb.PbList<PlayerJoinRejected> createRepeated() => $pb.PbList<PlayerJoinRejected>();
  @$core.pragma('dart2js:noInline')
  static PlayerJoinRejected getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PlayerJoinRejected>(create);
  static PlayerJoinRejected _defaultInstance;
}

class OfflineGameStarted extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OfflineGameStarted', createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  OfflineGameStarted._() : super();
  factory OfflineGameStarted() => create();
  factory OfflineGameStarted.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OfflineGameStarted.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OfflineGameStarted clone() => OfflineGameStarted()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OfflineGameStarted copyWith(void Function(OfflineGameStarted) updates) => super.copyWith((message) => updates(message as OfflineGameStarted)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OfflineGameStarted create() => OfflineGameStarted._();
  OfflineGameStarted createEmptyInstance() => create();
  static $pb.PbList<OfflineGameStarted> createRepeated() => $pb.PbList<OfflineGameStarted>();
  @$core.pragma('dart2js:noInline')
  static OfflineGameStarted getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OfflineGameStarted>(create);
  static OfflineGameStarted _defaultInstance;
}

class SavedGame extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SavedGame', createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', $pb.PbFieldType.OU3)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tag')
    ..hasRequiredFields = false
  ;

  SavedGame._() : super();
  factory SavedGame() => create();
  factory SavedGame.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SavedGame.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SavedGame clone() => SavedGame()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SavedGame copyWith(void Function(SavedGame) updates) => super.copyWith((message) => updates(message as SavedGame)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SavedGame create() => SavedGame._();
  SavedGame createEmptyInstance() => create();
  static $pb.PbList<SavedGame> createRepeated() => $pb.PbList<SavedGame>();
  @$core.pragma('dart2js:noInline')
  static SavedGame getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SavedGame>(create);
  static SavedGame _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get id => $_getIZ(0);
  @$pb.TagNumber(1)
  set id($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get tag => $_getSZ(1);
  @$pb.TagNumber(2)
  set tag($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTag() => $_has(1);
  @$pb.TagNumber(2)
  void clearTag() => clearField(2);
}

class SavedGamesResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SavedGamesResponse', createEmptyInstance: create)
    ..pc<SavedGame>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'savedGames', $pb.PbFieldType.PM, protoName: 'savedGames', subBuilder: SavedGame.create)
    ..hasRequiredFields = false
  ;

  SavedGamesResponse._() : super();
  factory SavedGamesResponse() => create();
  factory SavedGamesResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SavedGamesResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SavedGamesResponse clone() => SavedGamesResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SavedGamesResponse copyWith(void Function(SavedGamesResponse) updates) => super.copyWith((message) => updates(message as SavedGamesResponse)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SavedGamesResponse create() => SavedGamesResponse._();
  SavedGamesResponse createEmptyInstance() => create();
  static $pb.PbList<SavedGamesResponse> createRepeated() => $pb.PbList<SavedGamesResponse>();
  @$core.pragma('dart2js:noInline')
  static SavedGamesResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SavedGamesResponse>(create);
  static SavedGamesResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<SavedGame> get savedGames => $_getList(0);
}

class AttachCameraTarget extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AttachCameraTarget', createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'targetId', $pb.PbFieldType.OU3, protoName: 'targetId')
    ..hasRequiredFields = false
  ;

  AttachCameraTarget._() : super();
  factory AttachCameraTarget() => create();
  factory AttachCameraTarget.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AttachCameraTarget.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AttachCameraTarget clone() => AttachCameraTarget()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AttachCameraTarget copyWith(void Function(AttachCameraTarget) updates) => super.copyWith((message) => updates(message as AttachCameraTarget)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AttachCameraTarget create() => AttachCameraTarget._();
  AttachCameraTarget createEmptyInstance() => create();
  static $pb.PbList<AttachCameraTarget> createRepeated() => $pb.PbList<AttachCameraTarget>();
  @$core.pragma('dart2js:noInline')
  static AttachCameraTarget getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AttachCameraTarget>(create);
  static AttachCameraTarget _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get targetId => $_getIZ(0);
  @$pb.TagNumber(1)
  set targetId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTargetId() => $_has(0);
  @$pb.TagNumber(1)
  void clearTargetId() => clearField(1);
}

enum Gui2LoicMessage_Paylaod {
  playerJoined, 
  inputMove, 
  inputStopMove, 
  actionStart, 
  actionStop, 
  offlineGameStartedRequest, 
  savedGamesRequest, 
  notSet
}

class Gui2LoicMessage extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Gui2LoicMessage_Paylaod> _Gui2LoicMessage_PaylaodByTag = {
    2 : Gui2LoicMessage_Paylaod.playerJoined,
    3 : Gui2LoicMessage_Paylaod.inputMove,
    4 : Gui2LoicMessage_Paylaod.inputStopMove,
    5 : Gui2LoicMessage_Paylaod.actionStart,
    6 : Gui2LoicMessage_Paylaod.actionStop,
    7 : Gui2LoicMessage_Paylaod.offlineGameStartedRequest,
    8 : Gui2LoicMessage_Paylaod.savedGamesRequest,
    0 : Gui2LoicMessage_Paylaod.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Gui2LoicMessage', createEmptyInstance: create)
    ..oo(0, [2, 3, 4, 5, 6, 7, 8])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'playerId', protoName: 'playerId')
    ..aOM<PlayerJoined>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'playerJoined', protoName: 'playerJoined', subBuilder: PlayerJoined.create)
    ..aOM<InputMove>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'inputMove', protoName: 'inputMove', subBuilder: InputMove.create)
    ..aOM<InputStopMove>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'inputStopMove', protoName: 'inputStopMove', subBuilder: InputStopMove.create)
    ..aOM<ActionStart>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'actionStart', protoName: 'actionStart', subBuilder: ActionStart.create)
    ..aOM<ActionStop>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'actionStop', protoName: 'actionStop', subBuilder: ActionStop.create)
    ..aOM<OfflineGameStartedRequest>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'offlineGameStartedRequest', protoName: 'offlineGameStartedRequest', subBuilder: OfflineGameStartedRequest.create)
    ..aOM<SavedGamesRequest>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'savedGamesRequest', protoName: 'savedGamesRequest', subBuilder: SavedGamesRequest.create)
    ..hasRequiredFields = false
  ;

  Gui2LoicMessage._() : super();
  factory Gui2LoicMessage() => create();
  factory Gui2LoicMessage.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Gui2LoicMessage.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Gui2LoicMessage clone() => Gui2LoicMessage()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Gui2LoicMessage copyWith(void Function(Gui2LoicMessage) updates) => super.copyWith((message) => updates(message as Gui2LoicMessage)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Gui2LoicMessage create() => Gui2LoicMessage._();
  Gui2LoicMessage createEmptyInstance() => create();
  static $pb.PbList<Gui2LoicMessage> createRepeated() => $pb.PbList<Gui2LoicMessage>();
  @$core.pragma('dart2js:noInline')
  static Gui2LoicMessage getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Gui2LoicMessage>(create);
  static Gui2LoicMessage _defaultInstance;

  Gui2LoicMessage_Paylaod whichPaylaod() => _Gui2LoicMessage_PaylaodByTag[$_whichOneof(0)];
  void clearPaylaod() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get playerId => $_getSZ(0);
  @$pb.TagNumber(1)
  set playerId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPlayerId() => $_has(0);
  @$pb.TagNumber(1)
  void clearPlayerId() => clearField(1);

  @$pb.TagNumber(2)
  PlayerJoined get playerJoined => $_getN(1);
  @$pb.TagNumber(2)
  set playerJoined(PlayerJoined v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasPlayerJoined() => $_has(1);
  @$pb.TagNumber(2)
  void clearPlayerJoined() => clearField(2);
  @$pb.TagNumber(2)
  PlayerJoined ensurePlayerJoined() => $_ensure(1);

  @$pb.TagNumber(3)
  InputMove get inputMove => $_getN(2);
  @$pb.TagNumber(3)
  set inputMove(InputMove v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasInputMove() => $_has(2);
  @$pb.TagNumber(3)
  void clearInputMove() => clearField(3);
  @$pb.TagNumber(3)
  InputMove ensureInputMove() => $_ensure(2);

  @$pb.TagNumber(4)
  InputStopMove get inputStopMove => $_getN(3);
  @$pb.TagNumber(4)
  set inputStopMove(InputStopMove v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasInputStopMove() => $_has(3);
  @$pb.TagNumber(4)
  void clearInputStopMove() => clearField(4);
  @$pb.TagNumber(4)
  InputStopMove ensureInputStopMove() => $_ensure(3);

  @$pb.TagNumber(5)
  ActionStart get actionStart => $_getN(4);
  @$pb.TagNumber(5)
  set actionStart(ActionStart v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasActionStart() => $_has(4);
  @$pb.TagNumber(5)
  void clearActionStart() => clearField(5);
  @$pb.TagNumber(5)
  ActionStart ensureActionStart() => $_ensure(4);

  @$pb.TagNumber(6)
  ActionStop get actionStop => $_getN(5);
  @$pb.TagNumber(6)
  set actionStop(ActionStop v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasActionStop() => $_has(5);
  @$pb.TagNumber(6)
  void clearActionStop() => clearField(6);
  @$pb.TagNumber(6)
  ActionStop ensureActionStop() => $_ensure(5);

  @$pb.TagNumber(7)
  OfflineGameStartedRequest get offlineGameStartedRequest => $_getN(6);
  @$pb.TagNumber(7)
  set offlineGameStartedRequest(OfflineGameStartedRequest v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasOfflineGameStartedRequest() => $_has(6);
  @$pb.TagNumber(7)
  void clearOfflineGameStartedRequest() => clearField(7);
  @$pb.TagNumber(7)
  OfflineGameStartedRequest ensureOfflineGameStartedRequest() => $_ensure(6);

  @$pb.TagNumber(8)
  SavedGamesRequest get savedGamesRequest => $_getN(7);
  @$pb.TagNumber(8)
  set savedGamesRequest(SavedGamesRequest v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasSavedGamesRequest() => $_has(7);
  @$pb.TagNumber(8)
  void clearSavedGamesRequest() => clearField(8);
  @$pb.TagNumber(8)
  SavedGamesRequest ensureSavedGamesRequest() => $_ensure(7);
}

class PlayerJoined extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PlayerJoined', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'playerId', protoName: 'playerId')
    ..hasRequiredFields = false
  ;

  PlayerJoined._() : super();
  factory PlayerJoined() => create();
  factory PlayerJoined.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PlayerJoined.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PlayerJoined clone() => PlayerJoined()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PlayerJoined copyWith(void Function(PlayerJoined) updates) => super.copyWith((message) => updates(message as PlayerJoined)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PlayerJoined create() => PlayerJoined._();
  PlayerJoined createEmptyInstance() => create();
  static $pb.PbList<PlayerJoined> createRepeated() => $pb.PbList<PlayerJoined>();
  @$core.pragma('dart2js:noInline')
  static PlayerJoined getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PlayerJoined>(create);
  static PlayerJoined _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get playerId => $_getSZ(0);
  @$pb.TagNumber(1)
  set playerId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPlayerId() => $_has(0);
  @$pb.TagNumber(1)
  void clearPlayerId() => clearField(1);
}

class InputMove extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InputMove', createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'directionAngle', $pb.PbFieldType.OD, protoName: 'directionAngle')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'speed', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  InputMove._() : super();
  factory InputMove() => create();
  factory InputMove.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InputMove.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InputMove clone() => InputMove()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InputMove copyWith(void Function(InputMove) updates) => super.copyWith((message) => updates(message as InputMove)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InputMove create() => InputMove._();
  InputMove createEmptyInstance() => create();
  static $pb.PbList<InputMove> createRepeated() => $pb.PbList<InputMove>();
  @$core.pragma('dart2js:noInline')
  static InputMove getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InputMove>(create);
  static InputMove _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get directionAngle => $_getN(0);
  @$pb.TagNumber(1)
  set directionAngle($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDirectionAngle() => $_has(0);
  @$pb.TagNumber(1)
  void clearDirectionAngle() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get speed => $_getN(1);
  @$pb.TagNumber(2)
  set speed($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSpeed() => $_has(1);
  @$pb.TagNumber(2)
  void clearSpeed() => clearField(2);
}

class InputStopMove extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InputStopMove', createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  InputStopMove._() : super();
  factory InputStopMove() => create();
  factory InputStopMove.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InputStopMove.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InputStopMove clone() => InputStopMove()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InputStopMove copyWith(void Function(InputStopMove) updates) => super.copyWith((message) => updates(message as InputStopMove)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InputStopMove create() => InputStopMove._();
  InputStopMove createEmptyInstance() => create();
  static $pb.PbList<InputStopMove> createRepeated() => $pb.PbList<InputStopMove>();
  @$core.pragma('dart2js:noInline')
  static InputStopMove getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InputStopMove>(create);
  static InputStopMove _defaultInstance;
}

class ActionStart extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ActionStart', createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  ActionStart._() : super();
  factory ActionStart() => create();
  factory ActionStart.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ActionStart.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ActionStart clone() => ActionStart()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ActionStart copyWith(void Function(ActionStart) updates) => super.copyWith((message) => updates(message as ActionStart)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ActionStart create() => ActionStart._();
  ActionStart createEmptyInstance() => create();
  static $pb.PbList<ActionStart> createRepeated() => $pb.PbList<ActionStart>();
  @$core.pragma('dart2js:noInline')
  static ActionStart getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ActionStart>(create);
  static ActionStart _defaultInstance;
}

class ActionStop extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ActionStop', createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  ActionStop._() : super();
  factory ActionStop() => create();
  factory ActionStop.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ActionStop.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ActionStop clone() => ActionStop()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ActionStop copyWith(void Function(ActionStop) updates) => super.copyWith((message) => updates(message as ActionStop)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ActionStop create() => ActionStop._();
  ActionStop createEmptyInstance() => create();
  static $pb.PbList<ActionStop> createRepeated() => $pb.PbList<ActionStop>();
  @$core.pragma('dart2js:noInline')
  static ActionStop getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ActionStop>(create);
  static ActionStop _defaultInstance;
}

class OfflineGameStartedRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OfflineGameStartedRequest', createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gameId', $pb.PbFieldType.OU3, protoName: 'gameId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'playerName', protoName: 'playerName')
    ..hasRequiredFields = false
  ;

  OfflineGameStartedRequest._() : super();
  factory OfflineGameStartedRequest() => create();
  factory OfflineGameStartedRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OfflineGameStartedRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OfflineGameStartedRequest clone() => OfflineGameStartedRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OfflineGameStartedRequest copyWith(void Function(OfflineGameStartedRequest) updates) => super.copyWith((message) => updates(message as OfflineGameStartedRequest)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OfflineGameStartedRequest create() => OfflineGameStartedRequest._();
  OfflineGameStartedRequest createEmptyInstance() => create();
  static $pb.PbList<OfflineGameStartedRequest> createRepeated() => $pb.PbList<OfflineGameStartedRequest>();
  @$core.pragma('dart2js:noInline')
  static OfflineGameStartedRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OfflineGameStartedRequest>(create);
  static OfflineGameStartedRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get gameId => $_getIZ(0);
  @$pb.TagNumber(1)
  set gameId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasGameId() => $_has(0);
  @$pb.TagNumber(1)
  void clearGameId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get playerName => $_getSZ(1);
  @$pb.TagNumber(2)
  set playerName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPlayerName() => $_has(1);
  @$pb.TagNumber(2)
  void clearPlayerName() => clearField(2);
}

class SavedGamesRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SavedGamesRequest', createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  SavedGamesRequest._() : super();
  factory SavedGamesRequest() => create();
  factory SavedGamesRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SavedGamesRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SavedGamesRequest clone() => SavedGamesRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SavedGamesRequest copyWith(void Function(SavedGamesRequest) updates) => super.copyWith((message) => updates(message as SavedGamesRequest)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SavedGamesRequest create() => SavedGamesRequest._();
  SavedGamesRequest createEmptyInstance() => create();
  static $pb.PbList<SavedGamesRequest> createRepeated() => $pb.PbList<SavedGamesRequest>();
  @$core.pragma('dart2js:noInline')
  static SavedGamesRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SavedGamesRequest>(create);
  static SavedGamesRequest _defaultInstance;
}

class RenderPackElement extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RenderPackElement', createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'entityId', $pb.PbFieldType.OU3, protoName: 'entityId')
    ..aOM<Position>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'position', subBuilder: Position.create)
    ..aOM<Sprite>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'sprite', subBuilder: Sprite.create)
    ..hasRequiredFields = false
  ;

  RenderPackElement._() : super();
  factory RenderPackElement() => create();
  factory RenderPackElement.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RenderPackElement.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RenderPackElement clone() => RenderPackElement()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RenderPackElement copyWith(void Function(RenderPackElement) updates) => super.copyWith((message) => updates(message as RenderPackElement)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RenderPackElement create() => RenderPackElement._();
  RenderPackElement createEmptyInstance() => create();
  static $pb.PbList<RenderPackElement> createRepeated() => $pb.PbList<RenderPackElement>();
  @$core.pragma('dart2js:noInline')
  static RenderPackElement getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RenderPackElement>(create);
  static RenderPackElement _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get entityId => $_getIZ(0);
  @$pb.TagNumber(1)
  set entityId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEntityId() => $_has(0);
  @$pb.TagNumber(1)
  void clearEntityId() => clearField(1);

  @$pb.TagNumber(2)
  Position get position => $_getN(1);
  @$pb.TagNumber(2)
  set position(Position v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasPosition() => $_has(1);
  @$pb.TagNumber(2)
  void clearPosition() => clearField(2);
  @$pb.TagNumber(2)
  Position ensurePosition() => $_ensure(1);

  @$pb.TagNumber(3)
  Sprite get sprite => $_getN(2);
  @$pb.TagNumber(3)
  set sprite(Sprite v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasSprite() => $_has(2);
  @$pb.TagNumber(3)
  void clearSprite() => clearField(3);
  @$pb.TagNumber(3)
  Sprite ensureSprite() => $_ensure(2);
}

enum Component_Component {
  position, 
  velocity, 
  helath, 
  sprite, 
  notSet
}

class Component extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Component_Component> _Component_ComponentByTag = {
    1 : Component_Component.position,
    2 : Component_Component.velocity,
    3 : Component_Component.helath,
    4 : Component_Component.sprite,
    0 : Component_Component.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Component', createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4])
    ..aOM<Position>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'position', subBuilder: Position.create)
    ..aOM<Velocity>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'velocity', subBuilder: Velocity.create)
    ..aOM<Health>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'helath', subBuilder: Health.create)
    ..aOM<Sprite>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'sprite', subBuilder: Sprite.create)
    ..hasRequiredFields = false
  ;

  Component._() : super();
  factory Component() => create();
  factory Component.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Component.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Component clone() => Component()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Component copyWith(void Function(Component) updates) => super.copyWith((message) => updates(message as Component)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Component create() => Component._();
  Component createEmptyInstance() => create();
  static $pb.PbList<Component> createRepeated() => $pb.PbList<Component>();
  @$core.pragma('dart2js:noInline')
  static Component getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Component>(create);
  static Component _defaultInstance;

  Component_Component whichComponent() => _Component_ComponentByTag[$_whichOneof(0)];
  void clearComponent() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  Position get position => $_getN(0);
  @$pb.TagNumber(1)
  set position(Position v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasPosition() => $_has(0);
  @$pb.TagNumber(1)
  void clearPosition() => clearField(1);
  @$pb.TagNumber(1)
  Position ensurePosition() => $_ensure(0);

  @$pb.TagNumber(2)
  Velocity get velocity => $_getN(1);
  @$pb.TagNumber(2)
  set velocity(Velocity v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasVelocity() => $_has(1);
  @$pb.TagNumber(2)
  void clearVelocity() => clearField(2);
  @$pb.TagNumber(2)
  Velocity ensureVelocity() => $_ensure(1);

  @$pb.TagNumber(3)
  Health get helath => $_getN(2);
  @$pb.TagNumber(3)
  set helath(Health v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasHelath() => $_has(2);
  @$pb.TagNumber(3)
  void clearHelath() => clearField(3);
  @$pb.TagNumber(3)
  Health ensureHelath() => $_ensure(2);

  @$pb.TagNumber(4)
  Sprite get sprite => $_getN(3);
  @$pb.TagNumber(4)
  set sprite(Sprite v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasSprite() => $_has(3);
  @$pb.TagNumber(4)
  void clearSprite() => clearField(4);
  @$pb.TagNumber(4)
  Sprite ensureSprite() => $_ensure(3);
}

class Position extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Position', createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'x', $pb.PbFieldType.OD)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'y', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  Position._() : super();
  factory Position() => create();
  factory Position.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Position.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Position clone() => Position()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Position copyWith(void Function(Position) updates) => super.copyWith((message) => updates(message as Position)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Position create() => Position._();
  Position createEmptyInstance() => create();
  static $pb.PbList<Position> createRepeated() => $pb.PbList<Position>();
  @$core.pragma('dart2js:noInline')
  static Position getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Position>(create);
  static Position _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get x => $_getN(0);
  @$pb.TagNumber(1)
  set x($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasX() => $_has(0);
  @$pb.TagNumber(1)
  void clearX() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get y => $_getN(1);
  @$pb.TagNumber(2)
  set y($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasY() => $_has(1);
  @$pb.TagNumber(2)
  void clearY() => clearField(2);
}

class Velocity extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Velocity', createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxValue', $pb.PbFieldType.OD, protoName: 'maxValue')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  Velocity._() : super();
  factory Velocity() => create();
  factory Velocity.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Velocity.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Velocity clone() => Velocity()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Velocity copyWith(void Function(Velocity) updates) => super.copyWith((message) => updates(message as Velocity)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Velocity create() => Velocity._();
  Velocity createEmptyInstance() => create();
  static $pb.PbList<Velocity> createRepeated() => $pb.PbList<Velocity>();
  @$core.pragma('dart2js:noInline')
  static Velocity getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Velocity>(create);
  static Velocity _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get maxValue => $_getN(0);
  @$pb.TagNumber(1)
  set maxValue($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMaxValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearMaxValue() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get value => $_getN(1);
  @$pb.TagNumber(2)
  set value($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearValue() => clearField(2);
}

class Health extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Health', createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxValue', $pb.PbFieldType.OD, protoName: 'maxValue')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  Health._() : super();
  factory Health() => create();
  factory Health.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Health.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Health clone() => Health()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Health copyWith(void Function(Health) updates) => super.copyWith((message) => updates(message as Health)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Health create() => Health._();
  Health createEmptyInstance() => create();
  static $pb.PbList<Health> createRepeated() => $pb.PbList<Health>();
  @$core.pragma('dart2js:noInline')
  static Health getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Health>(create);
  static Health _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get maxValue => $_getN(0);
  @$pb.TagNumber(1)
  set maxValue($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMaxValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearMaxValue() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get value => $_getN(1);
  @$pb.TagNumber(2)
  set value($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearValue() => clearField(2);
}

class Sprite extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Sprite', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'state')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'scale', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  Sprite._() : super();
  factory Sprite() => create();
  factory Sprite.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Sprite.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Sprite clone() => Sprite()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Sprite copyWith(void Function(Sprite) updates) => super.copyWith((message) => updates(message as Sprite)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Sprite create() => Sprite._();
  Sprite createEmptyInstance() => create();
  static $pb.PbList<Sprite> createRepeated() => $pb.PbList<Sprite>();
  @$core.pragma('dart2js:noInline')
  static Sprite getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Sprite>(create);
  static Sprite _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get state => $_getSZ(0);
  @$pb.TagNumber(1)
  set state($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasState() => $_has(0);
  @$pb.TagNumber(1)
  void clearState() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get scale => $_getN(1);
  @$pb.TagNumber(2)
  set scale($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasScale() => $_has(1);
  @$pb.TagNumber(2)
  void clearScale() => clearField(2);
}

