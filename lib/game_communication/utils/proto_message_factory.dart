import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:protobuf/protobuf.dart' as Proto;

class ProtoMessageFactoryMethodNotRegistered implements Exception {
  String _message;
  ProtoMessageFactoryMethodNotRegistered(String message) : _message = message;
  String what() {
    return _message;
  }
}


/// Creates [Proto.Message] using factory methods
/// assigned to inscance of te class by [assignMethod] method and associated 
/// with [Proto] type.
/// Factory methods use [Map<String,dynamic> args] as parameter
/// and returns [Proto.Message].
class ProtoMessageFactory {
  Map<Type, Proto.Message Function(Map<String, dynamic> args)> __factories = {};

  /// Assigns factory method
  /// Overrides if exist.
  void assignMethod<T extends Proto.GeneratedMessage>(
      Proto.Message Function(Map<String, dynamic> args) factoryMethod) {
    __factories[T] = factoryMethod;
  }

  /// Creates Proto.Message ot [T] from map of args [args]
  /// throws [ProtoMessageFactoryMethodNotRegistered] if factory method not registed.
  Proto.Message create<T extends Proto.GeneratedMessage>(
      Map<String, dynamic> args) {
    if (__factories.keys.contains(T) == false)
      throw ProtoMessageFactoryMethodNotRegistered(
          "Factory method of $T type not registered!");
    return __factories[T](args);
  }
}
