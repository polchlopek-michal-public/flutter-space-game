import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;

class DeserialziationError implements Exception {
  String _message;
  DeserialziationError(String message) : _message = message;
  String what() {
    return _message;
  }
}


/// Tools of deserializarion and serializartion
/// of communication with protobuf.
class MessageDeserializationTool {

  /// Deserialzies message logic2gui
  static Proto.Logic2GuiMessage deserialzieLogic2Gui(dynamic rawMessage) {
    Proto.Message message = Proto.Message.fromBuffer(rawMessage);
    if (!message.hasLogic2GuiMessage()) {
      throw DeserialziationError(
          "Deserialziation error - not Logic2GuiMessage");
    }
    return message.logic2GuiMessage;
  }

  /// Deserialzies message gui2logic
  static Proto.Gui2LoicMessage deserialzieGui2Logic(dynamic rawMessage) {
    Proto.Message message = Proto.Message.fromBuffer(rawMessage);
    if (!message.hasLogic2GuiMessage()) {
      throw DeserialziationError(
          "Deserialziation error - not Gui2LogicMessage");
    }
    return message.gui2LoicMessage;
  }
}
