import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;

/// Abstract class which allows to communication
/// logic2gui and gui2logic
/// Implementations must provide method of sending and receiving Messages.
abstract class GameCommunication {
  void send(Proto.Message message);
  void recv(Proto.Message message);
  void setOnRecvHandler(Function handler) {
    throw UnimplementedError();
  }
}
