# Flutter Space Game

The practical part of Bachelor's thesis - **"Mobile game project with using "Dart" language and "Flutter"framework"**.

![](/docs/images/structure.png)  
The project structure splits game logic and UI. Both are placed in separated modules and communicate which each other using protobuf. 
The logical part has been implemented using  ECS (Entity Component System) design pattern. This part encapsulates also collision detection, local database and configuration. The graphical module renders map and sprites, which are updated when logical engine demands game state refresh.


# Screenshots
![](/docs/images/01.png)
![](/docs/images/02.png)