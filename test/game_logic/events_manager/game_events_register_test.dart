import 'package:flutter_test/flutter_test.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_register.dart';

import 'handlers_container_mock.dart';


void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('EventsRegisterFixture', () {
    GameEventsRegister eventsRegister;
    setUp(() {
      eventsRegister = GameEventsRegister();
    });

    test('Registering', () {
      HandlerContainerMock handlerContainerMock = HandlerContainerMock();
      eventsRegister.register<FakeGameEventOne>(handlerContainerMock.handlerOne);
      eventsRegister.register<FakeGameEventTwo>(handlerContainerMock.handlerTwo);
      eventsRegister.register<FakeGameEventThree>(handlerContainerMock.handlerThree);

      expect(eventsRegister.registered[FakeGameEventOne], handlerContainerMock.handlerOne);
      expect(eventsRegister.registered[FakeGameEventTwo], handlerContainerMock.handlerTwo);
      expect(eventsRegister.registered[FakeGameEventThree], handlerContainerMock.handlerThree);

    });
  });
}
