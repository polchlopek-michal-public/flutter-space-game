import 'package:flutter_test/flutter_test.dart';
import 'package:game/config/cofnig.dart';
import 'package:game/game_communication/game_communication.dart';
import 'package:game/game_logic/components/health.dart';
import 'package:game/game_logic/components/velocity.dart';
import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/components_manager/components_register.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_register.dart';
import 'package:game/game_logic/game_engine/game_event.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_manager.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_register.dart';
import 'package:game/game_logic/game_engine/systems_manager/i_system_manager.dart';
import 'package:game/game_persistance/i_game_persistance.dart';
import 'package:mockito/mockito.dart';

import 'handlers_container_mock.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('EventsManagerFixture', () {
    Config config;
    EntitiesManager entitiesManager;
    EntitiesRegister entitiesRegister;
    ComponentsManager componentsManager;
    ComponentsRegister componentsRegister;
    GameCommunication guiCommunication = GuiCommunicationMock();
    ISystemManager systemManager = SystemManagerMock();
    GameEventsRegister eventsRegister;
    GameEventsManager gameEventsManager;
    IGamePersistance gamePersistance = GamePersistanceMock();

    HandlerContainerMock handlersContainerMock;

    setUp(() {
      String rawConfig = '''
      app_name: "game"
      entities:
        - type: "Player"
          limit: 200
        - type: "Tentacle"
          limit: 200
      entities_limit: 1000
      ''';
      config = Config();
      config.loadFromString(rawConfig);

      entitiesRegister = EntitiesRegister(config: config);
      entitiesManager = EntitiesManager(
        entitiesRegister: entitiesRegister,
        config: config,
      );

      componentsRegister = ComponentsRegister()
        ..register<Health>()
        ..register<Velocity>();

      componentsManager = ComponentsManager(
        config: config,
        componentsRegister: componentsRegister,
      );

      handlersContainerMock = HandlerContainerMock();

      eventsRegister = GameEventsRegister()
        ..register<FakeGameEventOne>(handlersContainerMock.handlerOne)
        ..register<FakeGameEventTwo>(handlersContainerMock.handlerTwo)
        ..register<FakeGameEventThree>(handlersContainerMock.handlerThree);

      gameEventsManager = GameEventsManager(
        gameEventsRegister: eventsRegister,
        entitiesManager: entitiesManager,
        componentsManager: componentsManager,
        systemManager: systemManager,
        guiCommunication: guiCommunication,
        gamePersistance: gamePersistance,
      );
    });

    test('Calling registered events on event occurance', () {
      GameEvent e = FakeGameEventOne();
      gameEventsManager.event(FakeGameEventOne());
      verify(handlersContainerMock.handlerOne(
        e,
        entitiesManager,
        componentsManager,
        systemManager,
        gameEventsManager,
        guiCommunication,
        gamePersistance,
      ));

      GameEvent e1 = FakeGameEventOne();
      GameEvent e2 = FakeGameEventThree();
      gameEventsManager.event(e1);
      gameEventsManager.event(e2);

      verify(handlersContainerMock.handlerTwo(
        e1,
        entitiesManager,
        componentsManager,
        systemManager,
        gameEventsManager,
        guiCommunication,
        gamePersistance,
      ));

      verify(handlersContainerMock.handlerThree(
        e2,
        entitiesManager,
        componentsManager,
        systemManager,
        gameEventsManager,
        guiCommunication,
        gamePersistance,
      ));
    });
  });
}
