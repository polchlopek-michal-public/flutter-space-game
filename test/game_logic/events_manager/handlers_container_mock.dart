import 'package:game/game_communication/game_communication.dart';
import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_logic/game_engine/game_event.dart';
import 'package:game/game_logic/game_engine/game_events_manager/game_events_manager.dart';
import 'package:game/game_logic/game_engine/systems_manager/i_system_manager.dart';
import 'package:game/game_persistance/i_game_persistance.dart';
import 'package:mockito/mockito.dart';

abstract class HandlerContainer {
  void handlerOne(
    dynamic event,
    EntitiesManager entitiesManager,
    ComponentsManager componentsManager,
    ISystemManager systemManager,
    GameEventsManager gameEventsManager,
    GameCommunication gameCommunication,
    IGamePersistance gamePersistance,
  );
  void handlerTwo(
    dynamic event,
    EntitiesManager entitiesManager,
    ComponentsManager componentsManager,
    ISystemManager systemManager,
    GameEventsManager gameEventsManager,
    GameCommunication gameCommunication,
    IGamePersistance gamePersistance,
  );
  void handlerThree(
    dynamic event,
    EntitiesManager entitiesManager,
    ComponentsManager componentsManager,
    ISystemManager systemManager,
    GameEventsManager gameEventsManager,
    GameCommunication gameCommunication,
    IGamePersistance gamePersistance,
  );
}

class HandlerContainerMock extends Mock implements HandlerContainer {

}

class FakeGameEventOne extends GameEvent {}

class FakeGameEventTwo extends GameEvent {}

class FakeGameEventThree extends GameEvent {}

class GuiCommunicationMock extends Mock implements GameCommunication {}

class SystemManagerMock extends Mock implements ISystemManager {}

class GamePersistanceMock extends Mock implements IGamePersistance {}