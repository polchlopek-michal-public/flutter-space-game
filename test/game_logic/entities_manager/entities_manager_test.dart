import 'package:flutter_test/flutter_test.dart';
import 'package:game/config/cofnig.dart';
import 'package:game/game_logic/game_engine/entity.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_manager_errors.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_register.dart';

void main() {
  group('EntitiesManagerFixture', () {
    EntitiesManager entitiesManager;
    Config config;

    setUp(() {
      String rawConfig = '''
      app_name: "game"
      entities:
        - type: "Player"
          limit: 200
        - type: "Tentacle"
          limit: 200
      entities_limit: 1000
      ''';
      config = Config();
      config.loadFromString(rawConfig);

      entitiesManager = EntitiesManager(
        entitiesRegister: EntitiesRegister(config: config),
        config: config,
      );
    });

    // ---------------------------------------------------------------------

    test('Adding new entiities with no update', () {
      Entity e1 = entitiesManager.newEntity("Player");
      Entity e2 = entitiesManager.newEntity("Tentacle");
      Entity e3 = entitiesManager.newEntity();
      Entity e4 = entitiesManager.newEntity();
      Entity e5 = entitiesManager.newEntity("Player");

      expect(entitiesManager.entity(e1.id).id, 0);
      expect(entitiesManager.entity(e1.id).type, "Player");

      expect(entitiesManager.entity(e2.id).id, 200);
      expect(entitiesManager.entity(e2.id).type, "Tentacle");

      expect(entitiesManager.entity(e3.id).id, 400);
      expect(entitiesManager.entity(e3.id).type, "Unknown");

      expect(entitiesManager.entity(e4.id).id, 401);
      expect(entitiesManager.entity(e4.id).type, "Unknown");

      expect(entitiesManager.entity(e5.id).id, 1);
      expect(entitiesManager.entity(e5.id).type, "Player");
    });

    // ---------------------------------------------------------------------

    test('Remove entity', () {
      entitiesManager.newEntity("Player");
      expect(entitiesManager.entity(0).id, isNotNull);
      entitiesManager.remove(0);
      expect(entitiesManager.entities()[0], isNull);
    });

    test("Throwing error when can't find", () {
      expect(() {
        entitiesManager.entity(23);
      }, throwsA(isA<EntityNotFound>()));
    });

    test("Add entity - updating error", () {
      Entity e = entitiesManager.addEntity(Entity(id: 0));
      expect(entitiesManager.entity(0), e);
      expect(() {
        entitiesManager.addEntity(Entity(id: 0));
      }, throwsA(isA<UpdatingExisitngEntity>()));
      expect(entitiesManager.entity(0), e);
    });

    test("Add entity - adding type incompatible", () {
      expect(() {
        entitiesManager.addEntity(Entity(id: 0, type: "Tentacle"));
      }, throwsA(isA<TypeIncompatibilityError>()));
    });
  });
}
