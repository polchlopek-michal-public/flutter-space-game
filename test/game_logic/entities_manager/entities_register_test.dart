import 'package:flutter_test/flutter_test.dart';
import 'package:game/config/cofnig.dart';
import 'package:game/game_logic/game_engine/entities_manager/entities_register.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('EntitiesRegisterFixture', () {
    Config config = new Config();
    String rawConfig = ''' 
      app_name: "game"
      entities:
        - type: "Player"
          limit: 200
        - type: "Tentacle"
          limit: 200
      entities_limit: 1000
      ''';
    EntitiesRegister entitiesRegister;
    setUp(() {
      config.loadFromString(rawConfig);
      entitiesRegister = EntitiesRegister(config: config);
    });

    test('Incrementing nextAvaiableId', () {
      expect(entitiesRegister.nextAvaiableId, 1000);
    });

    test('Registering new entiiy types', () {
      expect(entitiesRegister.registered["Player"].begin, 0);
      expect(entitiesRegister.registered["Player"].end, 199);
      expect(entitiesRegister.registered["Tentacle"].begin, 200);
      expect(entitiesRegister.registered["Tentacle"].end, 399);
    });

    test('Checking type of suposed entiity of given id ', () {
      expect(entitiesRegister.typeof(0), "Player");
      expect(entitiesRegister.typeof(79), "Player");
      expect(entitiesRegister.typeof(199), "Player");
      expect(entitiesRegister.typeof(200), "Tentacle");
      expect(entitiesRegister.typeof(317), "Tentacle");
      expect(entitiesRegister.typeof(399), "Tentacle");
    });

    test('Summary limit of entiites greater than entities_limit', () {
      String rawConfig = ''' 
      app_name: "game"
      entities:
        - type: "Player"
          limit: 700
        - type: "Tentacle"
          limit: 800
      entities_limit: 1000
      ''';

      config.loadFromString(rawConfig);
      expect(() {
        EntitiesRegister(config: config);
      }, throwsA(isA<RegistrationError>()));
    });

    test('Registering unknown type', () {
      expect(entitiesRegister.typeof(400), "Unknown");
      expect(entitiesRegister.typeof(999), "Unknown");
      expect(entitiesRegister.typeof(1000), null);
    });
  });
}
