import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

import 'systems_manager_tests_mocks.dart';
import 'package:game/game_logic/game_engine/systems_manager/system_manager.dart';
import 'package:game/game_logic/systems_manager/system_manager_errors.dart';
import 'package:game/game_logic/game_engine/systems_manager/systems_register.dart';


void main() {
  group('EntitiesManagerFixture', () {
    GameEventsManagerMock gameEventsManager;
    SystemsRegister systemsRegister;
    SystemManager systemManager;

    SystemMockOne systemOne;
    SystemMockTwo systemTwo;
    SystemMockThree systemThree;

    setUp(() {
      gameEventsManager = GameEventsManagerMock();
      systemsRegister = SystemsRegister();

      systemOne = SystemMockOne();
      systemTwo = SystemMockTwo();
      systemThree = SystemMockThree();

      systemsRegister.register(systemOne);
      systemsRegister.register(systemTwo);
      systemsRegister.register(systemThree);

      systemManager = SystemManager(
        gameEventsManager: gameEventsManager,
        systemsRegister: systemsRegister,
      );
    });

    // ---------------------------------------------------------------------

    test('Systems updating', () {
      systemManager.update();

      verify(systemOne.update(gameEventsManager));
      verify(systemTwo.update(gameEventsManager));
      verify(systemThree.update(gameEventsManager));
    });

    test('Getting systems', () {
      expect(systemManager.system<SystemMockOne>(), systemOne);
      expect(systemManager.system<SystemMockTwo>(), systemTwo);
      expect(systemManager.system<SystemMockThree>(), systemThree);
    });

    test('Attaching new system', () {
      expect(() {
        systemManager.system<SystemMockFour>();
      }, throwsA(isA<SystemNotFound>()));

      SystemMockFour systemFour = SystemMockFour();
      systemManager.attach(systemFour);

      expect(systemManager.system<SystemMockFour>(), systemFour);
    });

    test('Detaching system', () {
      expect(systemManager.system<SystemMockOne>(), systemOne);

      systemManager.detach<SystemMockOne>();

      expect(() {
        systemManager.system<SystemMockOne>();
      }, throwsA(isA<SystemNotFound>()));
    });
  });
}
