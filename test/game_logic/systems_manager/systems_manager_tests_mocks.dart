import 'package:mockito/mockito.dart';
import 'package:game/game_logic/game_engine/game_events_manager/i_game_events_manager.dart';import 'package:game/game_logic/game_engine/system.dart';

class SystemMockOne extends Mock implements System {}

class SystemMockTwo extends Mock implements System {}

class SystemMockThree extends Mock implements System {}

class SystemMockFour extends Mock implements System {}

class GameEventsManagerMock extends Mock implements GameEventsManager {}
