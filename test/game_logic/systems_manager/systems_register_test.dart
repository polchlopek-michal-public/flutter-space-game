import 'package:flutter_test/flutter_test.dart';

import 'systems_manager_tests_mocks.dart';
import 'package:game/game_logic/game_engine/systems_manager/systems_register.dart';


void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('SystemsRegisterFixture', () {
    SystemsRegister systemsRegister;
    SystemMockOne systemOne;
    SystemMockTwo systemTwo;
    SystemMockThree systemThree;

    setUp(() {
      systemsRegister = SystemsRegister();

      systemOne = SystemMockOne();
      systemTwo = SystemMockTwo();
      systemThree = SystemMockThree();

      systemsRegister.register(systemOne);
      systemsRegister.register(systemTwo);
      systemsRegister.register(systemThree);
    });

    test('Registering new systems - HappyPath', () {
      expect(systemsRegister.registered[SystemMockOne], systemOne);
      expect(systemsRegister.registered[SystemMockTwo], systemTwo);
      expect(systemsRegister.registered[SystemMockThree], systemThree);
    });
  });
}
