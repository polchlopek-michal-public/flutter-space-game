import 'package:flutter_test/flutter_test.dart';
import 'package:game/config/cofnig.dart';
import 'package:game/game_logic/game_engine/component.dart';
import 'package:game/game_logic/components/health.dart';
import 'package:game/game_logic/components/velocity.dart';
import 'package:game/game_logic/game_engine/components_manager/components_manager.dart';
import 'package:game/game_logic/game_engine/components_manager/components_register.dart';

void main() {
  group('EntitiesManagerFixture', () {
    ComponentsManager componentsManager;
    ComponentsRegister componentsRegister;
    Config config;

    setUp(() {
      String rawConfig = '''
      app_name: "game"
      entities:
        - type: "Player"
          limit: 200
        - type: "Tentacle"
          limit: 200
      entities_limit: 1000
      ''';
      config = Config();
      config.loadFromString(rawConfig);

      componentsRegister = ComponentsRegister();
      componentsRegister.register<Health>();
      componentsRegister.register<Velocity>();

      componentsManager = ComponentsManager(
        config: config,
        componentsRegister: componentsRegister,
      );
    });

    // ---------------------------------------------------------------------

    test('Adding and getting component', () {
      Health health = Health(value: 100, maxValue: 100);
      componentsManager.add(health, 10);
      expect(componentsManager.component<Health>(10), health);
    });

    test('Getting pool of component', () {
      List<Velocity> velocities = [
        Velocity(speed: 1, x: 10, y: 10),
        Velocity(speed: 2, x: 15, y: 11),
        Velocity(speed: 1, x: 20, y: 30),
        Velocity(speed: 1.5, x: 2, y: 1),
      ];

      int entityCounter = 0;
      velocities.forEach((Velocity velocity) {
        componentsManager.add(velocity, entityCounter++);
      });

      expect(componentsManager.components<Velocity>(), velocities);
    });

    test('Getting all components of entity', () {
      List<Component> components1 = [
        Velocity(speed: 1, x: 10, y: 10),
        Health(maxValue: 30, value: 20),
      ];

      List<Component> components2 = [
        Health(maxValue: 250, value: 1),
      ];

      int e1 = 0;
      int e2 = 1;

      components1.forEach((Component component) {
        componentsManager.add(component, e1);
      });

      components2.forEach((Component component) {
        componentsManager.add(component, e2);
      });

      components1.forEach((Component component) {
        componentsManager.componentsOfEntity(e1).contains(component);
      });

      components2.forEach((Component component) {
        componentsManager.componentsOfEntity(e1).contains(component);
      });
    });

    test('Removing component', () {
      Health c1e1 = Health(value: 100, maxValue: 100);
      Velocity c2e1 = Velocity(speed: 1.5, x: -10, y: 15);
      Velocity c1e2 = Velocity(speed: 1, x: 20, y: 30);
      Health c2e2 = Health(value: 100, maxValue: 100);

      int entityId1 = 0;
      int entityId2 = 10;

      componentsManager.add(c1e1, entityId1);
      componentsManager.add(c2e1, entityId1);
      componentsManager.add(c1e2, entityId2);
      componentsManager.add(c2e2, entityId2);

      expect(componentsManager.components<Health>(), [c1e1, c2e2]);
      expect(componentsManager.components<Velocity>(), [c2e1, c1e2]);

      componentsManager.removeComponent<Velocity>(entityId2);

      expect(componentsManager.components<Health>(), [c1e1, c2e2]);
      expect(componentsManager.components<Velocity>(), [c2e1]);
    });

    test('Removing all components of entity', () {
      Health c1e1 = Health(value: 100, maxValue: 100);
      Velocity c2e1 = Velocity(speed: 1.5, x: -10, y: 15);
      Velocity c1e2 = Velocity(speed: 1, x: 20, y: 30);
      Health c2e2 = Health(value: 100, maxValue: 100);

      int entityId1 = 0;
      int entityId2 = 10;

      componentsManager.add(c1e1, entityId1);
      componentsManager.add(c2e1, entityId1);
      componentsManager.add(c1e2, entityId2);
      componentsManager.add(c2e2, entityId2);

      expect(componentsManager.components<Health>(), [c1e1, c2e2]);
      expect(componentsManager.components<Velocity>(), [c2e1, c1e2]);

      componentsManager.removeComponentsOfEntity(entityId2);

      expect(componentsManager.components<Health>(), [c1e1]);
      expect(componentsManager.components<Velocity>(), [c2e1]);
    });
    // ---------------------------------------------------------------------
  });
}
