import 'package:flutter_test/flutter_test.dart';
import 'package:game/game_logic/game_engine/component.dart';
import 'package:game/game_logic/game_engine/components_manager/components_register.dart';

import 'package:game/game_logic/components/health.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('EntitiesRegisterFixture', () {
    ComponentsRegister componentsRegister;
    setUp(() {
      componentsRegister = ComponentsRegister();
    });

    test('Registering new Component - HappyPath', () {
      componentsRegister.register<Health>();
      expect(componentsRegister.registered.contains(Health), true);
      expect(componentsRegister.registered.contains(Component), false);
    });

    test('Double register', () {
      componentsRegister.register<Health>();
      componentsRegister.register<Health>();
      expect(componentsRegister.registered.length, 1);
    });
  });
}
