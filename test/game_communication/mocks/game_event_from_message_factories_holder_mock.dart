import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_logic/game_engine/game_event.dart';
import 'package:mockito/mockito.dart';

class FakeGameEvent1 implements GameEvent {
  
} 

abstract class GameEventFromMessageFactoriesHolder {
  GameEvent factoryMethod1(Proto.Gui2LoicMessage playerJoinedMessage);
  GameEvent factoryMethod2(Proto.Gui2LoicMessage playerJoinedMessage);
  GameEvent factoryMethod3(Proto.Gui2LoicMessage playerJoinedMessage);
  GameEvent factoryMethod4(Proto.Gui2LoicMessage playerJoinedMessage);
}

class GameEventFromMessageFactoriesHolderMock extends Mock
    implements GameEventFromMessageFactoriesHolder {}
