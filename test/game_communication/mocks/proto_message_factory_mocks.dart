import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:mockito/mockito.dart';

abstract class ProtoMessageFactoriesHolder {
  Proto.Message playerJoinedFactory(Map<String, dynamic> args);
  Proto.Message inputMoveFactory(Map<String, dynamic> args);
  Proto.Message actionStartFactory(Map<String, dynamic> args);
  Proto.Message actionStopFactory(Map<String, dynamic> args);
}

class ProtoMessageFactoriesHolderMock extends Mock
    implements ProtoMessageFactoriesHolder {}
