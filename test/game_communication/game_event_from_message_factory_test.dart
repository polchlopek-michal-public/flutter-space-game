import 'package:flutter_test/flutter_test.dart';
import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_logic/communication/utils/game_event_from_message_factory.dart';
import 'package:mockito/mockito.dart';

import 'mocks/game_event_from_message_factories_holder_mock.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  GameEventFromMessageFactory gameEventFromMessageFactory;
  GameEventFromMessageFactoriesHolderMock factoriesHolderMock =
      GameEventFromMessageFactoriesHolderMock();

  when(factoriesHolderMock.factoryMethod1(any)).thenReturn(FakeGameEvent1());

  group('GameEventFromMessageFactoryFixture', () {
    setUp(() {
      gameEventFromMessageFactory = GameEventFromMessageFactory()
        ..assignMethod(Proto.Gui2LoicMessage_Paylaod.playerJoined,
            factoriesHolderMock.factoryMethod1)
        ..assignMethod(Proto.Gui2LoicMessage_Paylaod.inputMove,
            factoriesHolderMock.factoryMethod2)
        ..assignMethod(Proto.Gui2LoicMessage_Paylaod.actionStart,
            factoriesHolderMock.factoryMethod3)
        ..assignMethod(Proto.Gui2LoicMessage_Paylaod.actionStop,
            factoriesHolderMock.factoryMethod4);
    });

    test('Creating GameEvent sing factory - HappyPath', () {
      Proto.Gui2LoicMessage fakeMessage = new Proto.Gui2LoicMessage();
      expect(
          gameEventFromMessageFactory
              .create(Proto.Gui2LoicMessage_Paylaod.playerJoined, fakeMessage)
              .runtimeType,
          FakeGameEvent1);
      verify(factoriesHolderMock.factoryMethod1(fakeMessage));
    });
  });
}
