import 'package:flutter_test/flutter_test.dart';
import 'package:game/game_communication/generated/lib/game_communication/messages.pb.dart'
    as Proto;
import 'package:game/game_communication/utils/proto_message_factory.dart';
import 'package:mockito/mockito.dart';

import 'mocks/proto_message_factory_mocks.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  ProtoMessageFactory protoMessageFactory;
  ProtoMessageFactoriesHolderMock factoriesHolderMock =
      ProtoMessageFactoriesHolderMock();

  Proto.Message playerJoinedMessage = Proto.Message();
  playerJoinedMessage.gui2LoicMessage = Proto.Gui2LoicMessage();
  playerJoinedMessage.gui2LoicMessage.playerJoined = Proto.PlayerJoined();

  when(factoriesHolderMock.playerJoinedFactory(any))
      .thenReturn(playerJoinedMessage);
  when(factoriesHolderMock.inputMoveFactory(any)).thenReturn(Proto.Message());
  when(factoriesHolderMock.actionStartFactory(any)).thenReturn(Proto.Message());
  when(factoriesHolderMock.actionStopFactory(any)).thenReturn(Proto.Message());

  group('ProtoMessageFactoryFixture', () {
    setUp(() {
      protoMessageFactory = ProtoMessageFactory()
        ..assignMethod<Proto.PlayerJoined>(
            factoriesHolderMock.playerJoinedFactory)
        ..assignMethod<Proto.InputMove>(factoriesHolderMock.inputMoveFactory)
        ..assignMethod<Proto.ActionStart>(
            factoriesHolderMock.actionStartFactory);
    });

    test('Creating GameEvent sing factory - HappyPath', () {
      expect(protoMessageFactory.create<Proto.PlayerJoined>({}),
          playerJoinedMessage);
      verify(factoriesHolderMock.playerJoinedFactory({}));
    });

    test('Creating GameEvent sing factory - creating unregistered', () {
      expect(() {
        protoMessageFactory.create<Proto.ActionStop>({});
      }, throwsA(isA<ProtoMessageFactoryMethodNotRegistered>()));
    });
  });
}
