import 'package:flutter_test/flutter_test.dart';
import 'package:game/config/cofnig.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('ConfigFixture', () {
    Config config = new Config();
    setUp(() {});

    test('Load config from asset', () async {
      await config.loadFromAsset();
      expect(config["app_name"], "game");
    });

    test('Load config from String', () {
      String rawConfig = '''
      app_name: "game"
      entities:
        - type: "Player"
          limit: 200
        - type: "Tentacle"
          limit: 300
      ''';
      config.loadFromString(rawConfig);
      expect(config["app_name"], "game");
      expect(config["entities"][0]["type"], "Player");
      expect(config["entities"][0]["limit"], 200);
      expect(config["entities"][1]["type"], "Tentacle");
      expect(config["entities"][1]["limit"], 300);
    });
  });
}
